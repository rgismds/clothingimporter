﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.dlgFileOpen = New System.Windows.Forms.OpenFileDialog()
        Me.sspStatus = New System.Windows.Forms.StatusStrip()
        Me.tslStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tspb = New System.Windows.Forms.ToolStripProgressBar()
        Me.txtOutput = New System.Windows.Forms.TextBox()
        Me.radDev = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.radLive = New System.Windows.Forms.RadioButton()
        Me.grpClothing = New System.Windows.Forms.GroupBox()
        Me.btnDashDataClothingRebuild = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtFileNameAndPathIPClothing = New System.Windows.Forms.TextBox()
        Me.btnClothingImportShrink = New System.Windows.Forms.Button()
        Me.btnClothingImportSales = New System.Windows.Forms.Button()
        Me.btnClothingImport = New System.Windows.Forms.Button()
        Me.grpDailyReport = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtFileNameAndPathDailyRpt = New System.Windows.Forms.TextBox()
        Me.btnDailyRptImport = New System.Windows.Forms.Button()
        Me.grpSuperCatVerifier = New System.Windows.Forms.GroupBox()
        Me.btnSupercatVerifyAllFiles = New System.Windows.Forms.Button()
        Me.btnImportSuperCat = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtFileNameAndPathSuperCat = New System.Windows.Forms.TextBox()
        Me.btnSupercatVerify = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtOPFileName = New System.Windows.Forms.TextBox()
        Me.dlgFolder = New System.Windows.Forms.FolderBrowserDialog()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.sspStatus.SuspendLayout()
        Me.grpClothing.SuspendLayout()
        Me.grpDailyReport.SuspendLayout()
        Me.grpSuperCatVerifier.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(1405, 473)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(4)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(104, 41)
        Me.btnExit.TabIndex = 0
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'sspStatus
        '
        Me.sspStatus.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.sspStatus.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tslStatusLabel, Me.tspb})
        Me.sspStatus.Location = New System.Drawing.Point(0, 555)
        Me.sspStatus.Name = "sspStatus"
        Me.sspStatus.Padding = New System.Windows.Forms.Padding(1, 0, 19, 0)
        Me.sspStatus.Size = New System.Drawing.Size(1548, 25)
        Me.sspStatus.TabIndex = 4
        Me.sspStatus.Text = "StatusStrip1"
        '
        'tslStatusLabel
        '
        Me.tslStatusLabel.Name = "tslStatusLabel"
        Me.tslStatusLabel.Size = New System.Drawing.Size(154, 20)
        Me.tslStatusLabel.Text = "ToolStripStatusLabel1"
        '
        'tspb
        '
        Me.tspb.Name = "tspb"
        Me.tspb.Size = New System.Drawing.Size(150, 19)
        Me.tspb.Step = 5
        Me.tspb.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.tspb.Visible = False
        '
        'txtOutput
        '
        Me.txtOutput.Location = New System.Drawing.Point(1092, 47)
        Me.txtOutput.Margin = New System.Windows.Forms.Padding(4)
        Me.txtOutput.Multiline = True
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtOutput.Size = New System.Drawing.Size(416, 399)
        Me.txtOutput.TabIndex = 5
        '
        'radDev
        '
        Me.radDev.AutoSize = True
        Me.radDev.Checked = True
        Me.radDev.Location = New System.Drawing.Point(108, 12)
        Me.radDev.Margin = New System.Windows.Forms.Padding(4)
        Me.radDev.Name = "radDev"
        Me.radDev.Size = New System.Drawing.Size(17, 16)
        Me.radDev.TabIndex = 6
        Me.radDev.TabStop = True
        Me.radDev.UseVisualStyleBackColor = True
        Me.radDev.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(51, 15)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(36, 17)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "DEV"
        Me.Label2.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(151, 15)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(34, 17)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Live"
        Me.Label3.Visible = False
        '
        'radLive
        '
        Me.radLive.AutoSize = True
        Me.radLive.Location = New System.Drawing.Point(208, 12)
        Me.radLive.Margin = New System.Windows.Forms.Padding(4)
        Me.radLive.Name = "radLive"
        Me.radLive.Size = New System.Drawing.Size(17, 16)
        Me.radLive.TabIndex = 8
        Me.radLive.UseVisualStyleBackColor = True
        Me.radLive.Visible = False
        '
        'grpClothing
        '
        Me.grpClothing.Controls.Add(Me.btnDashDataClothingRebuild)
        Me.grpClothing.Controls.Add(Me.Label1)
        Me.grpClothing.Controls.Add(Me.txtFileNameAndPathIPClothing)
        Me.grpClothing.Controls.Add(Me.btnClothingImportShrink)
        Me.grpClothing.Controls.Add(Me.btnClothingImportSales)
        Me.grpClothing.Controls.Add(Me.btnClothingImport)
        Me.grpClothing.Location = New System.Drawing.Point(53, 47)
        Me.grpClothing.Margin = New System.Windows.Forms.Padding(4)
        Me.grpClothing.Name = "grpClothing"
        Me.grpClothing.Padding = New System.Windows.Forms.Padding(4)
        Me.grpClothing.Size = New System.Drawing.Size(1019, 177)
        Me.grpClothing.TabIndex = 15
        Me.grpClothing.TabStop = False
        Me.grpClothing.Text = "Clothing Import"
        '
        'btnDashDataClothingRebuild
        '
        Me.btnDashDataClothingRebuild.Location = New System.Drawing.Point(528, 112)
        Me.btnDashDataClothingRebuild.Margin = New System.Windows.Forms.Padding(4)
        Me.btnDashDataClothingRebuild.Name = "btnDashDataClothingRebuild"
        Me.btnDashDataClothingRebuild.Size = New System.Drawing.Size(435, 44)
        Me.btnDashDataClothingRebuild.TabIndex = 21
        Me.btnDashDataClothingRebuild.Text = "Clothing data rebuild on DD"
        Me.btnDashDataClothingRebuild.UseVisualStyleBackColor = True
        Me.btnDashDataClothingRebuild.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(443, 36)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 17)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "Input file"
        '
        'txtFileNameAndPathIPClothing
        '
        Me.txtFileNameAndPathIPClothing.Location = New System.Drawing.Point(547, 32)
        Me.txtFileNameAndPathIPClothing.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFileNameAndPathIPClothing.Multiline = True
        Me.txtFileNameAndPathIPClothing.Name = "txtFileNameAndPathIPClothing"
        Me.txtFileNameAndPathIPClothing.Size = New System.Drawing.Size(416, 58)
        Me.txtFileNameAndPathIPClothing.TabIndex = 18
        '
        'btnClothingImportShrink
        '
        Me.btnClothingImportShrink.Location = New System.Drawing.Point(211, 98)
        Me.btnClothingImportShrink.Margin = New System.Windows.Forms.Padding(4)
        Me.btnClothingImportShrink.Name = "btnClothingImportShrink"
        Me.btnClothingImportShrink.Size = New System.Drawing.Size(131, 59)
        Me.btnClothingImportShrink.TabIndex = 17
        Me.btnClothingImportShrink.Text = "Clothing Importer Shrink only"
        Me.btnClothingImportShrink.UseVisualStyleBackColor = True
        '
        'btnClothingImportSales
        '
        Me.btnClothingImportSales.Location = New System.Drawing.Point(211, 32)
        Me.btnClothingImportSales.Margin = New System.Windows.Forms.Padding(4)
        Me.btnClothingImportSales.Name = "btnClothingImportSales"
        Me.btnClothingImportSales.Size = New System.Drawing.Size(131, 59)
        Me.btnClothingImportSales.TabIndex = 16
        Me.btnClothingImportSales.Text = "Clothing Importer Sales only"
        Me.btnClothingImportSales.UseVisualStyleBackColor = True
        '
        'btnClothingImport
        '
        Me.btnClothingImport.Location = New System.Drawing.Point(23, 62)
        Me.btnClothingImport.Margin = New System.Windows.Forms.Padding(4)
        Me.btnClothingImport.Name = "btnClothingImport"
        Me.btnClothingImport.Size = New System.Drawing.Size(173, 95)
        Me.btnClothingImport.TabIndex = 15
        Me.btnClothingImport.Text = "Clothing Importer (combined Sales and shrink in 1 xls) Old version"
        Me.btnClothingImport.UseVisualStyleBackColor = True
        Me.btnClothingImport.Visible = False
        '
        'grpDailyReport
        '
        Me.grpDailyReport.Controls.Add(Me.Label6)
        Me.grpDailyReport.Controls.Add(Me.txtFileNameAndPathDailyRpt)
        Me.grpDailyReport.Controls.Add(Me.btnDailyRptImport)
        Me.grpDailyReport.Location = New System.Drawing.Point(53, 231)
        Me.grpDailyReport.Margin = New System.Windows.Forms.Padding(4)
        Me.grpDailyReport.Name = "grpDailyReport"
        Me.grpDailyReport.Padding = New System.Windows.Forms.Padding(4)
        Me.grpDailyReport.Size = New System.Drawing.Size(1019, 127)
        Me.grpDailyReport.TabIndex = 16
        Me.grpDailyReport.TabStop = False
        Me.grpDailyReport.Text = "Daily Rpt"
        Me.grpDailyReport.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(443, 27)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 17)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "Input file"
        '
        'txtFileNameAndPathDailyRpt
        '
        Me.txtFileNameAndPathDailyRpt.Location = New System.Drawing.Point(547, 23)
        Me.txtFileNameAndPathDailyRpt.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFileNameAndPathDailyRpt.Multiline = True
        Me.txtFileNameAndPathDailyRpt.Name = "txtFileNameAndPathDailyRpt"
        Me.txtFileNameAndPathDailyRpt.Size = New System.Drawing.Size(416, 58)
        Me.txtFileNameAndPathDailyRpt.TabIndex = 20
        Me.txtFileNameAndPathDailyRpt.Text = "C:\_WIP\MarchWeb\Clients\RGIS\Clothing data 201201.xlsx"
        '
        'btnDailyRptImport
        '
        Me.btnDailyRptImport.Location = New System.Drawing.Point(117, 36)
        Me.btnDailyRptImport.Margin = New System.Windows.Forms.Padding(4)
        Me.btnDailyRptImport.Name = "btnDailyRptImport"
        Me.btnDailyRptImport.Size = New System.Drawing.Size(173, 59)
        Me.btnDailyRptImport.TabIndex = 11
        Me.btnDailyRptImport.Text = "Daily report Importer"
        Me.btnDailyRptImport.UseVisualStyleBackColor = True
        '
        'grpSuperCatVerifier
        '
        Me.grpSuperCatVerifier.Controls.Add(Me.btnSupercatVerifyAllFiles)
        Me.grpSuperCatVerifier.Controls.Add(Me.btnImportSuperCat)
        Me.grpSuperCatVerifier.Controls.Add(Me.Label5)
        Me.grpSuperCatVerifier.Controls.Add(Me.txtFileNameAndPathSuperCat)
        Me.grpSuperCatVerifier.Controls.Add(Me.btnSupercatVerify)
        Me.grpSuperCatVerifier.ForeColor = System.Drawing.SystemColors.Control
        Me.grpSuperCatVerifier.Location = New System.Drawing.Point(53, 384)
        Me.grpSuperCatVerifier.Margin = New System.Windows.Forms.Padding(4)
        Me.grpSuperCatVerifier.Name = "grpSuperCatVerifier"
        Me.grpSuperCatVerifier.Padding = New System.Windows.Forms.Padding(4)
        Me.grpSuperCatVerifier.Size = New System.Drawing.Size(1019, 129)
        Me.grpSuperCatVerifier.TabIndex = 17
        Me.grpSuperCatVerifier.TabStop = False
        Me.grpSuperCatVerifier.Text = "SuperCat Verifier"
        '
        'btnSupercatVerifyAllFiles
        '
        Me.btnSupercatVerifyAllFiles.ForeColor = System.Drawing.Color.Black
        Me.btnSupercatVerifyAllFiles.Location = New System.Drawing.Point(0, 0)
        Me.btnSupercatVerifyAllFiles.Margin = New System.Windows.Forms.Padding(4)
        Me.btnSupercatVerifyAllFiles.Name = "btnSupercatVerifyAllFiles"
        Me.btnSupercatVerifyAllFiles.Size = New System.Drawing.Size(163, 50)
        Me.btnSupercatVerifyAllFiles.TabIndex = 18
        Me.btnSupercatVerifyAllFiles.Text = "Pick SuperCat Verifier Folder (ALL files).."
        Me.btnSupercatVerifyAllFiles.UseVisualStyleBackColor = True
        '
        'btnImportSuperCat
        '
        Me.btnImportSuperCat.Enabled = False
        Me.btnImportSuperCat.ForeColor = System.Drawing.Color.Black
        Me.btnImportSuperCat.Location = New System.Drawing.Point(937, 0)
        Me.btnImportSuperCat.Name = "btnImportSuperCat"
        Me.btnImportSuperCat.Size = New System.Drawing.Size(75, 35)
        Me.btnImportSuperCat.TabIndex = 18
        Me.btnImportSuperCat.Text = "Import"
        Me.btnImportSuperCat.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(443, 18)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 17)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Input file"
        '
        'txtFileNameAndPathSuperCat
        '
        Me.txtFileNameAndPathSuperCat.Location = New System.Drawing.Point(211, 0)
        Me.txtFileNameAndPathSuperCat.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFileNameAndPathSuperCat.Multiline = True
        Me.txtFileNameAndPathSuperCat.Name = "txtFileNameAndPathSuperCat"
        Me.txtFileNameAndPathSuperCat.Size = New System.Drawing.Size(710, 50)
        Me.txtFileNameAndPathSuperCat.TabIndex = 16
        '
        'btnSupercatVerify
        '
        Me.btnSupercatVerify.ForeColor = System.Drawing.Color.Black
        Me.btnSupercatVerify.Location = New System.Drawing.Point(32, 71)
        Me.btnSupercatVerify.Margin = New System.Windows.Forms.Padding(4)
        Me.btnSupercatVerify.Name = "btnSupercatVerify"
        Me.btnSupercatVerify.Size = New System.Drawing.Size(100, 50)
        Me.btnSupercatVerify.TabIndex = 12
        Me.btnSupercatVerify.Text = "Pick SuperCat Verify (single file).."
        Me.btnSupercatVerify.UseVisualStyleBackColor = True
        Me.btnSupercatVerify.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(507, 496)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(93, 17)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "OP File name"
        Me.Label4.Visible = False
        '
        'txtOPFileName
        '
        Me.txtOPFileName.Location = New System.Drawing.Point(611, 492)
        Me.txtOPFileName.Margin = New System.Windows.Forms.Padding(4)
        Me.txtOPFileName.Multiline = True
        Me.txtOPFileName.Name = "txtOPFileName"
        Me.txtOPFileName.Size = New System.Drawing.Size(416, 47)
        Me.txtOPFileName.TabIndex = 14
        Me.txtOPFileName.Text = "C:\Kue\Programming\Development\RGIS\ClothingImporter\SuperCatVerifier.csv"
        Me.txtOPFileName.Visible = False
        '
        'dlgFolder
        '
        Me.dlgFolder.RootFolder = System.Environment.SpecialFolder.MyComputer
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(261, 362)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(246, 17)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "SuperCat Verifier source import folder"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1548, 580)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.grpSuperCatVerifier)
        Me.Controls.Add(Me.grpDailyReport)
        Me.Controls.Add(Me.grpClothing)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.radLive)
        Me.Controls.Add(Me.txtOPFileName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.radDev)
        Me.Controls.Add(Me.txtOutput)
        Me.Controls.Add(Me.sspStatus)
        Me.Controls.Add(Me.btnExit)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Clothing & SuperCat Verifier Importer"
        Me.sspStatus.ResumeLayout(False)
        Me.sspStatus.PerformLayout()
        Me.grpClothing.ResumeLayout(False)
        Me.grpClothing.PerformLayout()
        Me.grpDailyReport.ResumeLayout(False)
        Me.grpDailyReport.PerformLayout()
        Me.grpSuperCatVerifier.ResumeLayout(False)
        Me.grpSuperCatVerifier.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents dlgFileOpen As System.Windows.Forms.OpenFileDialog
    Friend WithEvents sspStatus As System.Windows.Forms.StatusStrip
    Friend WithEvents tslStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtOutput As System.Windows.Forms.TextBox
    Friend WithEvents radDev As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents radLive As System.Windows.Forms.RadioButton
    Friend WithEvents grpClothing As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtFileNameAndPathIPClothing As System.Windows.Forms.TextBox
    Friend WithEvents btnClothingImportShrink As System.Windows.Forms.Button
    Friend WithEvents btnClothingImportSales As System.Windows.Forms.Button
    Friend WithEvents btnClothingImport As System.Windows.Forms.Button
    Friend WithEvents grpDailyReport As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtFileNameAndPathDailyRpt As System.Windows.Forms.TextBox
    Friend WithEvents btnDailyRptImport As System.Windows.Forms.Button
    Friend WithEvents grpSuperCatVerifier As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtFileNameAndPathSuperCat As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtOPFileName As System.Windows.Forms.TextBox
    Friend WithEvents btnSupercatVerify As System.Windows.Forms.Button
    Friend WithEvents btnSupercatVerifyAllFiles As System.Windows.Forms.Button
    Friend WithEvents dlgFolder As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents btnDashDataClothingRebuild As System.Windows.Forms.Button
    Friend WithEvents btnImportSuperCat As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tspb As System.Windows.Forms.ToolStripProgressBar
End Class
