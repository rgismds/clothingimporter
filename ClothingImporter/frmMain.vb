﻿Imports System.IO.File
Imports System.IO
Imports Microsoft.Office.Interop
Imports System.Data.SqlClient
Imports System.Text
Imports System.Configuration
Imports System.Collections.Specialized

Public Class frmMain

    Dim sDefaultSuperCatFolder As String

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        sDefaultSuperCatFolder = ConfigurationManager.AppSettings("DefaultSuperCatFolder")
        txtFileNameAndPathSuperCat.Text = sDefaultSuperCatFolder

    End Sub
    Public Property StatusBarText() As String
        Get
            Return tslStatusLabel.Text
        End Get
        Set(ByVal value As String)
            tslStatusLabel.Text = value
            System.Windows.Forms.Application.DoEvents()
        End Set
    End Property

    Private m_DBConn As SqlConnection
    Public Property DBConn() As SqlConnection
        Get
            Return m_DBConn
        End Get
        Set(ByVal value As SqlConnection)
            m_DBConn = value
        End Set
    End Property

    Private mConn As String = ""

    Private Sub btnexit_click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.close()
    End Sub


    'Private Sub btnClothingImport_Click(sender As System.Object, e As System.EventArgs) Handles btnClothingImport.Click

    '    '   Import data into Sansbury's Clothing Staging table

    '    Dim xnWSCount As Integer = 0
    '    Dim xsRtnMsg As String = ""

    '    Dim xadDates() As Date = Nothing
    '    Dim xanDateCol() As Integer = Nothing
    '    Dim xanDateValSales() As Decimal = Nothing
    '    Dim xasDateCodeCol() As String = Nothing
    '    'Dim xanDateValExposure() As Decimal = Nothing
    '    Dim xanDateValClExpAdj() As Decimal = Nothing
    '    Dim xanDateValRGISExpAdj() As Decimal = Nothing

    '    Try

    '        txtOutput.Text = "Output" & vbCrLf

    '        '   Identify the file to import
    '        dlgFileOpen.FileName = txtFileNameAndPathIPClothing.Text
    '        dlgFileOpen.ShowDialog()
    '        txtFileNameAndPathIPClothing.Text = dlgFileOpen.FileName

    '        If txtFileNameAndPathIPClothing.Text.Length = 0 Then
    '            MessageBox.Show("Missing file name. Job cancelled.")
    '            Return
    '        End If

    '        If Not File.Exists(txtFileNameAndPathIPClothing.Text) Then
    '            MessageBox.Show("File name not found. Job cancelled.")
    '            Return
    '        End If

    '        '   Get the DB Connection string
    '        '   Currently this is via the Radio buttons to vary Dev/Live. The client may only be Sainsburys
    '        'Dim xsClientName As String
    '        'If radDev.Checked Then
    '        '    xsClientName = "SSDev"
    '        'Else
    '        '    xsClientName = "SS"
    '        'End If

    '        Dim xbValid As Boolean = False
    '        Dim xoLic = New RGISLic.LicVal

    '        'If xoLic.GetDBForClient(xsClientName, "Data Source=smartspacedatacenter.com\BOE140;Initial Catalog=RGIS_Dashboard; Integrated Security=False; User ID=sa; Password=CjC7vHYB2yESaBgq") Then
    '        '    xbValid = True
    '        'Else
    '        '    '   If bad, then exit
    '        '    MessageBox.Show("Error getting the DB details: " & xoLic.RtnMsg)
    '        '    Exit Sub
    '        'End If





    '        '   Build the connection string
    '        'Dim xsConnStr As String = "Data Source=" & xoLic.DBLocn & ";Initial Catalog=" & xoLic.DBName & "; Integrated Security=False; User ID=sa; Password=CjC7vHYB2yESaBgq"
    '        'Dim xsConnStr As String = "Server=CND42412H2\STPL_08;Initial Catalog=Sainsburys;Integrated Security=SSPI;"
    '        xbValid = True
    '        '   Clear the work tables
    '        DBConn = New SqlConnection(mConn)
    '        Dim xoCmd As New SqlCommand

    '        DBConn.Open()


    '        '   Open the workbook
    '        Dim xoExcel As Excel.Application
    '        Dim xoExWS As New Excel.Worksheet
    '        Dim xoExWB As Excel.Workbook

    '        StatusBarText = "Opening Excel file. "

    '        xoExcel = New Excel.Application

    '        xoExWB = xoExcel.Workbooks.Open(txtFileNameAndPathIPClothing.Text)
    '        xoExcel.Visible = True
    '        xoExcel.Application.DisplayAlerts = False

    '        StatusBarText = "Processing the worksheets."

    '        ' Jeremy Rogers - 15/08/2017
    '        ' Changed code so that it only reads data from last sheet in the Workbook
    '        xoExWS = xoExWB.Worksheets(xoExWB.Worksheets.Count)

    '        For Each xoExWS In xoExWB.Worksheets

    '            '   Work through each worksheet in the workbook
    '            '   There will be a number of sheets for Sales and a number for Shrink
    '            '   The different types have different column layouts as the Shrink has 2 columns per day

    '            Dim xbSales As Boolean = False
    '            Dim xsWSName As String = xoExWS.Name
    '            Dim xbDatesFound As Boolean = False
    '            Dim xnDateCount As Integer = 0

    '            ReDim xadDates(xnDateCount)
    '            ReDim xanDateCol(xnDateCount)
    '            ReDim xasDateCodeCol(xnDateCount)
    '            ReDim xanDateValSales(xnDateCount)
    '            'ReDim xanDateValExposure(xnDateCount)
    '            ReDim xanDateValClExpAdj(xnDateCount)
    '            ReDim xanDateValRGISExpAdj(xnDateCount)

    '            '   Determine the sheet type
    '            Dim xnWrk As Integer = xsWSName.ToUpper.IndexOf("SALES")
    '            If xnWrk > 0 Then
    '                xbSales = True
    '            End If
    '            txtOutput.Text += xoExWS.Name & " " & xbSales & " " & vbCrLf
    '            StatusBarText = "Processing worksheet " & xoExWS.Name & "."

    '            '   Miss out Sales for testing
    '            If Not xbSales Then '###############zzzzzzzzzzzzzzzzzz Remove this! ToDo


    '                '   Get the date range
    '                Dim xsDate As String = ""
    '                Dim xnDateStartCol As Integer = 6
    '                Dim xnDateStartRow As Integer = 1
    '                Dim xnDateEndCol As Integer = 0
    '                Dim xnRow As Integer = 1
    '                Dim xnCol As Integer = 1
    '                Dim xnColForThisDate As Integer = 0

    '                '   Find the "Day" row
    '                '   The word "Day" appears in the cell preceding the row of dates. This can be in any row from 5-10 and any column from E(5) to K(11)
    '                xnRow = 5
    '                Dim xbFound As Boolean = False
    '                For xnRow = 5 To 10
    '                    For xnCol = 5 To 11
    '                        If Not IsNothing(xoExWS.Cells(xnRow, xnCol).value) AndAlso xoExWS.Cells(xnRow, xnCol).value.ToString.ToUpper = "DAY" Then
    '                            xbFound = True
    '                            Exit For
    '                        End If
    '                    Next
    '                    If xbFound Then Exit For
    '                Next

    '                '   Confirm that the word "Reason" is two cells below this. We use this to deduce what type of shrink transaction is being given
    '                If Not xbSales Then
    '                    If IsNothing(xoExWS.Cells(xnRow + 2, xnCol).value) OrElse xoExWS.Cells(xnRow + 2, xnCol).value.ToString.ToUpper <> "REASON" Then
    '                        MessageBox.Show("The 'Reason' row wasn't found two rows below 'Day'. The worksheet " & xsWSName & " is not in the correct format")
    '                        Exit Sub
    '                    End If
    '                End If


    '                '   Now extract the dates from the cells. There can be 1-3 columns per date with no rhyme or reason as to why (!)
    '                '   We need to record what date is relevant to each column so that the figures on each row can be totalled for the table update.
    '                '   The assumption is that all columns for a given date are to be added together and that they appear together (i.e. are not jumbled up in the column order)
    '                xnDateStartCol = xnCol + 1
    '                xnDateStartRow = xnRow
    '                Dim xdsvDate As Date = Nothing
    '                Dim xdWrk As Date = Nothing
    '                xnDateCount = -1

    '                For xnCol = xnDateStartCol To 30

    '                    If IsNothing(CellData(xnRow, xnCol)) Then
    '                        Exit For
    '                    End If

    '                    xsDate = CellData(xnRow, xnCol)
    '                    If xsDate.Length = 0 Or Not IsDate(xsDate) Then
    '                        Exit For
    '                    End If

    '                    xdWrk = CDate(xsDate)
    '                    If IsNothing(xdsvDate) OrElse xdsvDate <> xdWrk Then

    '                        xnDateCount += 1
    '                        ReDim Preserve xadDates(xnDateCount)
    '                        Dim xdDate As Date = CDate(xsDate)
    '                        xadDates(xnDateCount) = xdDate
    '                        xbDatesFound = True
    '                        xdsvDate = xdWrk

    '                    End If

    '                    ReDim Preserve xanDateCol(xnCol)
    '                    xanDateCol(xnCol) = xnDateCount

    '                    If Not xbSales Then
    '                        ReDim Preserve xasDateCodeCol(xnCol)
    '                        xasDateCodeCol(xnCol) = xoExWS.Cells(xnRow + 2, xnCol).value
    '                    End If

    '                Next

    '                xnDateEndCol = xnCol

    '                'For xnRow = xnDateStartRow To 21
    '                '    xsDate = xoExWS.Cells(xnRow, xnDateStartCol).value
    '                '    If IsDate(xsDate) Then

    '                '        xadDates(xnDateCount) = CDate(xsDate)
    '                '        xnDateStartCol += 1

    '                '        '   Get the other 6 dates in this row
    '                '        For xnCol = xnDateStartCol To 22
    '                '            xnDateCount += 1
    '                '            ReDim Preserve xadDates(xnDateCount)
    '                '            xsDate = xoExWS.Cells(xnRow, xnCol).value
    '                '            If xsDate.Length > 0 And IsDate(xsDate) Then
    '                '                Dim xdDate As Date = CDate(xsDate)
    '                '                xadDates(xnDateCount) = xdDate
    '                '            End If

    '                '            '   Cater for the extra cols in the shrink w/s
    '                '            If xbSales Then
    '                '                If xnCol = 12 Then Exit For
    '                '            Else
    '                '                xnCol += 1
    '                '            End If
    '                '        Next

    '                '        xbDatesFound = True
    '                '        Exit For
    '                '    End If
    '                'Next

    '                If Not xbDatesFound Then
    '                    xsRtnMsg += "No dates found on worksheet " & xsWSName
    '                    txtOutput.Text += "No dates found in " & xsWSName & vbCrLf
    '                    Exit For
    '                End If

    '                '   Extract the data
    '                Dim xnRowCount As Integer = xoExWS.Rows.Count
    '                xnRow = 1
    '                Do While CellData(xnRow, 1).value <> "SSL" And xnRow < xnRowCount
    '                    xnRow += 1
    '                Loop

    '                If xnRow >= xnRowCount Then
    '                    txtOutput.Text += "No data found in " & xsWSName & vbCrLf
    '                End If

    '                '   First data row found so process all the following non blank rows
    '                Do While xnRow < xnRowCount And CellData(xnRow, 1).ToString().Length > 0
    '                    Dim xdDate As Date? = Nothing
    '                    Dim xsStore As String = ""
    '                    Dim xnStore As Integer = 0

    '                    'xnRow += 1
    '                    xsRtnMsg = "a" & xnRow

    '                    xsStore = CellData(xnRow, 4)
    '                    If Not IsNothing(xsStore) And xsStore.Length > 0 Then
    '                        xnStore = CInt(xsStore)
    '                        If xnStore > 0 Then
    '                            '   Get the figure for each day
    '                            '   Note that (at this time) there will be an output row for each day, even for zero values
    '                            '   The s-p Inserts if there's no row there or updates if one does exist
    '                            '   We work through all columns to accumulate date and then output all dates at one time

    '                            If xbSales Then
    '                                ReDim xanDateValSales(xadDates.GetUpperBound(0))
    '                            Else
    '                                'ReDim xanDateValExposure(xadDates.GetUpperBound(0))
    '                                ReDim xanDateValClExpAdj(xadDates.GetUpperBound(0))
    '                                ReDim xanDateValRGISExpAdj(xadDates.GetUpperBound(0))
    '                            End If

    '                            For xnCol = xnDateStartCol To xnDateEndCol - 1

    '                                Dim xnVal As Decimal = 0
    '                                Dim xsWrk As String = CellData(xnRow, xnCol)
    '                                If Not IsNothing(xsWrk) AndAlso xsWrk.Length > 0 Then
    '                                    xnVal = Val(xsWrk)
    '                                End If

    '                                If xbSales Then
    '                                    xanDateValSales(xanDateCol(xnCol)) += xnVal
    '                                Else
    '                                    'xanDateValExposure(xanDateCol(xnCol)) += xnVal
    '                                    Select Case xasDateCodeCol(xnCol)
    '                                        Case Is = "900" : xanDateValClExpAdj(xanDateCol(xnCol)) += xnVal
    '                                        Case Is = "902" : xanDateValClExpAdj(xanDateCol(xnCol)) += xnVal
    '                                        Case Is = "903" : xanDateValRGISExpAdj(xanDateCol(xnCol)) += xnVal
    '                                    End Select

    '                                End If

    '                            Next

    '                            For xnWrk = 0 To xnDateCount - 1
    '                                xoCmd = New SqlCommand
    '                                xoCmd.Connection = DBConn
    '                                xoCmd.CommandType = CommandType.StoredProcedure

    '                                Try
    '                                    xoCmd.CommandText = "p_StageClothingInsertOrUpdate"
    '                                    xoCmd.Parameters.Add("MetricDate", SqlDbType.DateTime).Value = xadDates(xnWrk)
    '                                    xoCmd.Parameters.Add("StoreNumber", SqlDbType.Int).Value = xnStore
    '                                    If xbSales Then
    '                                        xoCmd.Parameters.Add("TotalSales", SqlDbType.Decimal).Value = xanDateValSales(xnWrk)
    '                                        'xoCmd.Parameters.Add("TotalExposure", SqlDbType.Decimal).Value = Nothing
    '                                        xoCmd.Parameters.Add("ClientExposureAdj", SqlDbType.Decimal).Value = Nothing
    '                                        xoCmd.Parameters.Add("RGISExposureAdj", SqlDbType.Decimal).Value = Nothing
    '                                    Else
    '                                        xoCmd.Parameters.Add("TotalSales", SqlDbType.Decimal).Value = Nothing
    '                                        'xoCmd.Parameters.Add("TotalExposure", SqlDbType.Decimal).Value = xanDateValExposure(xnWrk)
    '                                        xoCmd.Parameters.Add("ClientExposureAdj", SqlDbType.Decimal).Value = xanDateValClExpAdj(xnWrk)
    '                                        xoCmd.Parameters.Add("RGISExposureAdj", SqlDbType.Decimal).Value = xanDateValRGISExpAdj(xnWrk)
    '                                    End If
    '                                    xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output
    '                                    xoCmd.ExecuteNonQuery()

    '                                    Dim xsWrk As String = xoCmd.Parameters("RowsUpdated").Value.ToString
    '                                    If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then
    '                                        Dim xnWrk2 As Integer = CInt(xsWrk)
    '                                        If xnWrk2 <> 1 Then
    '                                            xsRtnMsg += "Problem during p_StageClothingInsert/Update. "
    '                                            xbValid = False
    '                                        End If
    '                                    End If
    '                                Catch ex As Exception
    '                                    xsRtnMsg += "Problem when updating data for Row " & xnRow & " in sheet " & xsWSName & ". Error " & ex.Message & " "
    '                                End Try

    '                            Next


    '                            'End If





    '                            'xnCol = -1

    '                            'For xnDateCount = 0 To 6

    '                            '    Dim xsWrk As String = ""
    '                            '    Dim xnVal As Decimal = 0

    '                            '    xnCol += 1

    '                            '    xsRtnMsg += "b"

    '                            '    xsWrk = xoExWS.Cells(xnRow, 6 + xnCol).value
    '                            '    If Not IsNothing(xsWrk) AndAlso xsWrk.Length > 0 Then
    '                            '        xnVal = Val(xsWrk)
    '                            '    End If

    '                            '    '   Add the extra Shrink column
    '                            '    If Not xbSales Then
    '                            '        xnCol += 1
    '                            '        xsWrk = xoExWS.Cells(xnRow, 6 + xnCol).value
    '                            '        If Not IsNothing(xsWrk) AndAlso xsWrk.Length > 0 Then
    '                            '            xnVal += Val(xsWrk)
    '                            '        End If
    '                            '    End If

    '                            '    xoCmd = New SqlCommand
    '                            '    xoCmd.Connection = DBConn
    '                            '    xoCmd.CommandType = CommandType.StoredProcedure
    '                            '    Try

    '                            '        xoCmd.CommandText = "p_StageClothingInsertOrUpdate"
    '                            '        xoCmd.Parameters.Add("MetricDate", SqlDbType.DateTime).Value = xadDates(xnDateCount)
    '                            '        xoCmd.Parameters.Add("StoreNumber", SqlDbType.Int).Value = xnStore
    '                            '        xoCmd.Parameters.Add("TotalSales", SqlDbType.Decimal).Value = xnVal
    '                            '        xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output
    '                            '        xoCmd.ExecuteNonQuery()

    '                            '        xsWrk = xoCmd.Parameters("RowsUpdated").Value.ToString
    '                            '        If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then
    '                            '            xnWrk = CInt(xsWrk)
    '                            '            If xnWrk <> 1 Then
    '                            '                xsRtnMsg += "Problem during p_StageClothingInsert/Update. "
    '                            '                xbValid = False
    '                            '            End If
    '                            '        End If
    '                            '    Catch ex As Exception
    '                            '        xsRtnMsg += "Problem when updating data for Row " & xnRow & " in sheet " & xsWSName & ". Error " & ex.Message & " "
    '                            '    End Try

    '                            '    Next

    '                            'Else

    '                            'End If

    '                            xsRtnMsg += "c"

    '                        End If
    '                    End If
    '                    xnRow += 1
    '                    If CellData(xnRow, 1).value <> "SSL" Then
    '                        Exit Do
    '                    End If
    '                Loop
    '            End If
    '        Next

    '        xoExWB.Close()
    '        xoExcel.Quit()
    '        xoExWS = Nothing
    '        xoExWB = Nothing
    '        xoExcel = Nothing

    '        txtOutput.Text += "Done." & vbCrLf

    '        MessageBox.Show("Done")

    '    Catch ex As Exception
    '        MessageBox.Show("Error: " & ex.Message & ": " & xsRtnMsg)
    '    Finally
    '        If Not IsNothing(DBConn) AndAlso DBConn.State <> ConnectionState.Closed Then
    '            DBConn.Close()
    '        End If

    '    End Try

    'End Sub

    Private Sub btnDailyRptImport_Click(sender As System.Object, e As System.EventArgs) Handles btnDailyRptImport.Click

        '   Process the Daily Report

        Dim xnWSCount As Integer = 0
        Dim xsRtnMsg As String = ""
        Dim xbOKToProcess As Boolean = False
        Dim xbProcessThisSheet As Boolean = False
        Dim xsIPFileName As String = ""
        Dim xsOPFileName As String = ""

        Dim xadDates() As Date = Nothing
        Dim xanDateCol() As Integer = Nothing
        Dim xanDateValSales() As Decimal = Nothing
        Dim xasDateCodeCol() As String = Nothing
        'Dim xanDateValExposure() As Decimal = Nothing
        Dim xanDateValClExpAdj() As Decimal = Nothing
        Dim xanDateValRGISExpAdj() As Decimal = Nothing


        '   Get the file name
        Try

            txtOutput.Text = "Import Daily Report:" & vbCrLf

            '   Identify the file to import
            dlgFileOpen.FileName = txtFileNameAndPathDailyRpt.Text
            dlgFileOpen.ShowDialog()
            txtFileNameAndPathDailyRpt.Text = dlgFileOpen.FileName
            xsIPFileName = txtFileNameAndPathDailyRpt.Text
            xsOPFileName = xsIPFileName.Replace("\ClientData\", "\ClientData_Processed\")

            If txtFileNameAndPathDailyRpt.Text.Length = 0 Then
                MessageBox.Show("Missing file name. Job cancelled.")
                Return
            End If

            If Not File.Exists(txtFileNameAndPathDailyRpt.Text) Then
                MessageBox.Show("File name not found. Job cancelled.")
                Return
            End If
        Catch ex As Exception
            MessageBox.Show("Problem getting the excel file . Job cancelled.")
            Return
        End Try


        '   Open the file and confirm it is the right one - by checking the worksheets within it
        '   Open the workbook
        Dim xoExcel As Excel.Application = Nothing
        Dim xoExWS As New Excel.Worksheet
        Dim xoExWB As Excel.Workbook = Nothing

        Try

            StatusBarText = "Opening Excel file. "

            xoExcel = New Excel.Application

            xoExWB = xoExcel.Workbooks.Open(txtFileNameAndPathDailyRpt.Text)
            xoExcel.Visible = True
            xoExcel.Application.DisplayAlerts = False

            StatusBarText = "Processing the worksheets."

            Dim xbFoundME As Boolean = False
            Dim xbFoundCS As Boolean = False
            Dim xbFoundPFS As Boolean = False
            Dim xbFoundPIQA As Boolean = False

            For Each xoExWS In xoExWB.Worksheets

                '   Work through each worksheet in the workbook
                '   There are the following w/s expected:
                '       MAIN ESTATE, C STORE, PFS, PIQA
                '   There may be more w/s but as long as the above are found we will process the workbook

                Dim xsWSName As String = xoExWS.Name.ToUpper
                xbProcessThisSheet = False

                Select Case xsWSName
                    Case Is = "MAIN ESTATE" : xbFoundME = True
                    Case Is = "C STORE" : xbFoundCS = True
                    Case Is = "PFS" : xbFoundPFS = True
                    Case Is = "PIQA" : xbFoundPIQA = True
                End Select

            Next

            If Not xbFoundME Or Not xbFoundCS Or Not xbFoundPFS Or Not xbFoundPIQA Then
                MessageBox.Show("Missing worksheet in workbook. The following are expected:  MAIN ESTATE, C STORE, PFS, PIQA")
                Return
            End If

            '   Get the DB Connection string
            '   Currently this is via the Radio buttons to vary Dev/Live. The client may only be Sainsburys
            'Dim xsClientName As String
            'If radDev.Checked Then
            '    xsClientName = "SSDev"
            'Else
            '    xsClientName = "SS"
            'End If

            Dim xbValid As Boolean = False
            Dim xoLic = New RGISLic.LicVal

            'If xoLic.GetDBForClient(xsClientName, "Data Source=smartspacedatacenter.com\BOE140;Initial Catalog=RGIS_Dashboard; Integrated Security=False; User ID=sa; Password=CjC7vHYB2yESaBgq") Then
            '    xbValid = True
            'Else
            '    '   If bad, then exit
            '    MessageBox.Show("Error getting the DB details: " & xoLic.RtnMsg)
            '    Exit Sub
            'End If


            '   Build the connection string
            'Dim xsConnStr As String = "Data Source=" & xoLic.DBLocn & ";Initial Catalog=" & xoLic.DBName & "; Integrated Security=False; User ID=sa; Password=CjC7vHYB2yESaBgq"

            ' Dim xsConnStr As String = "Server=CND42412H2\STPL_08;Initial Catalog=Sainsburys;Integrated Security=SSPI;"
            xbValid = True
            '   Clear the work tables
            DBConn = New SqlConnection(mConn)
            Dim xoCmd As New SqlCommand

            DBConn.Open()

            '   Process the data in the workbook. Each worksheet contains different types of data.

            For Each xoExWS In xoExWB.Worksheets

                Dim xsWSName As String = xoExWS.Name.ToUpper
                Dim xnRow As Integer = 3
                Dim xnCol As Integer = 1

                Do While Not IsNothing(xoExWS.Cells(xnRow, xnCol).value) AndAlso xoExWS.Cells(xnRow, xnCol).value.ToString.Length > 0

                    Dim xdDate As DateTime = Nothing

                    If IsDate(xoExWS.Cells(xnRow, xnCol).value) Then
                        xdDate = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                    End If

                    Try
                        xoCmd = New SqlCommand
                        xoCmd.Connection = DBConn
                        xoCmd.CommandType = CommandType.StoredProcedure

                        Select Case xsWSName
                            '   -------------------------------------------------------------------------------------------------------------------------------------------------
                            Case Is = "MAIN ESTATE", "C STORE"
                                '   Process Main Estate
                                '   Create and fill the vars
                                'Dim xdDate As DateTime = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)

                                xbProcessThisSheet = True
                                xnCol = 1
                                Dim xdCountDate As DateTime = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                xnCol = 2
                                Dim xnDist As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                xnCol = 3
                                Dim xnStoreNumber As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                xnCol = 4
                                Dim xsLocation As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                xnCol = 5
                                Dim xnZone As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                xnCol = 6   '   F
                                Dim xnRegion As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                xnCol = 8   '   H
                                Dim xnTotalUnitAccuracy As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                xnCol = 13   '   M
                                Dim xnRMVUnitAccuracy As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                xnCol = 14
                                Dim xnTotalSKUAccuracy As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                xnCol = 15
                                Dim xnRMVSKUAccuracy As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                xnCol = 16
                                Dim xnPcntSKUsCheckedInBackRoom As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                xnCol = 17
                                Dim xnPcntSKUsCheckedOnSalesfloor As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                xnCol = 18
                                Dim xnTotalNoUnitsDeletedAtCloseout As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                xnCol = 19
                                Dim xnNoRMVUsed As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                xnCol = 20  '   T
                                Dim xnPcntOfAreasChecked As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                xnCol = 22  '   V
                                Dim xnTotalUnitsChecked As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                xnCol = 23
                                Dim xnNoOfAreasCounted As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                xnCol = 24
                                Dim xnPcntOfUnitsChecked As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                xnCol = 25
                                Dim xnTotalSKUsChecked As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                xnCol = 26
                                Dim xnPcntOfSKUsChecked As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                xnCol = 27
                                Dim xdSFStartTime As DateTime = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, CombineDateAndTime(xdCountDate, xoExWS.Cells(xnRow, xnCol).value, Nothing))
                                xnCol = 28
                                Dim xdSFFinishTime As DateTime = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, CombineDateAndTime(xdCountDate, xoExWS.Cells(xnRow, xnCol).value, xdSFStartTime))
                                xnCol = 29
                                Dim xdLeaveStoreTime As DateTime? = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, CombineDateAndTime(xdCountDate, xoExWS.Cells(xnRow, xnCol).value, xdSFStartTime))
                                xnCol = 30
                                Dim xdSFCountLength As Long = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, ConvertDecimalFractionToLong(xoExWS.Cells(xnRow, xnCol).value))
                                xnCol = 31
                                Dim xdLastXmitTime As DateTime = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, CombineDateAndTime(xdCountDate, xoExWS.Cells(xnRow, xnCol).value, xdSFStartTime))
                                xnCol = 32
                                Dim xdDeltaLastXmitAndSFFinish As Long = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, ConvertDecimalFractionToLong(xoExWS.Cells(xnRow, xnCol).value))
                                xnCol = 33
                                Dim xdTotalTimeInStore As Long = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, ConvertDecimalFractionToLong(xoExWS.Cells(xnRow, xnCol).value))
                                xnCol = 34
                                Dim xnEstimatesValue As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                xnCol = 35
                                Dim xnActualValue As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                xnCol = 36
                                Dim xnVariance As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                xnCol = 37  '   AK
                                Dim xnVariancePcnt As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                xnCol = 39  '   AM
                                Dim xnPcntOfBackRoom As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value * 100)
                                xnCol = 40
                                Dim xnBackRoomPrepGrade As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                xnCol = 41
                                Dim xnSalesFloorPrepGuide As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                xnCol = 42
                                Dim xsStoreManagerName As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                xnCol = 43
                                Dim xsRGISSupervisorName As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                xnCol = 44  '   AR
                                Dim xbStoreWalkCompleted As Boolean = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, IIf(xoExWS.Cells(xnRow, xnCol).value = "Y", True, False))
                                xnCol = 46  '   AT
                                Dim xbDidStoreRcvFullSetVarianceRpts As Boolean = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, IIf(xoExWS.Cells(xnRow, xnCol).value = "Y", True, False))
                                xnCol = 47  '   AU
                                Dim xbEnvLeftWithManager As Boolean = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, IIf(xoExWS.Cells(xnRow, xnCol).value = "Y", True, False))

                                Try
                                    xoCmd.CommandText = "p_Stage_DailyReport_MainConv_Insert"
                                    xoCmd.Parameters.Add("Worksheet", SqlDbType.NVarChar).Value = xsWSName
                                    xoCmd.Parameters.Add("CountDate", SqlDbType.DateTime).Value = xdCountDate
                                    xoCmd.Parameters.Add("Dist", SqlDbType.Int).Value = xnDist
                                    xoCmd.Parameters.Add("StoreNumber", SqlDbType.Int).Value = xnStoreNumber
                                    xoCmd.Parameters.Add("Location", SqlDbType.NVarChar).Value = xsLocation
                                    xoCmd.Parameters.Add("Zone", SqlDbType.Int).Value = xnZone
                                    xoCmd.Parameters.Add("Region", SqlDbType.Int).Value = xnRegion
                                    xoCmd.Parameters.Add("TotalUnitAccuracy", SqlDbType.Decimal).Value = xnTotalUnitAccuracy
                                    xoCmd.Parameters.Add("RMVUnitAccuracy", SqlDbType.Decimal).Value = xnRMVUnitAccuracy
                                    xoCmd.Parameters.Add("TotalSKUAccuracy", SqlDbType.Decimal).Value = xnTotalSKUAccuracy
                                    xoCmd.Parameters.Add("RMVSKUAccuracy", SqlDbType.Decimal).Value = xnRMVSKUAccuracy
                                    xoCmd.Parameters.Add("PcntSKUsCheckedInBackRoom", SqlDbType.Decimal).Value = xnPcntSKUsCheckedInBackRoom
                                    xoCmd.Parameters.Add("PcntSKUsCheckedOnSalesfloor", SqlDbType.Decimal).Value = xnPcntSKUsCheckedOnSalesfloor
                                    xoCmd.Parameters.Add("TotalNoUnitsDeletedAtCloseout", SqlDbType.Int).Value = xnTotalNoUnitsDeletedAtCloseout
                                    xoCmd.Parameters.Add("NoRMVUsed", SqlDbType.Int).Value = xnNoRMVUsed
                                    xoCmd.Parameters.Add("PcntOfAreasChecked", SqlDbType.Decimal).Value = xnPcntOfAreasChecked
                                    xoCmd.Parameters.Add("TotalUnitsChecked", SqlDbType.Int).Value = xnTotalUnitsChecked
                                    xoCmd.Parameters.Add("NoOfAreasCounted", SqlDbType.Int).Value = xnNoOfAreasCounted
                                    xoCmd.Parameters.Add("PcntOfUnitsChecked", SqlDbType.Decimal).Value = xnPcntOfUnitsChecked
                                    xoCmd.Parameters.Add("TotalSKUsChecked", SqlDbType.Int).Value = xnTotalSKUsChecked
                                    xoCmd.Parameters.Add("PcntOfSKUsChecked", SqlDbType.Decimal).Value = xnPcntOfSKUsChecked
                                    xoCmd.Parameters.Add("SFStartTime", SqlDbType.DateTime).Value = xdSFStartTime
                                    xoCmd.Parameters.Add("SFFinishTime", SqlDbType.DateTime).Value = xdSFFinishTime
                                    xoCmd.Parameters.Add("LeaveStoreTime", SqlDbType.DateTime).Value = IIf(IsNothing(xoExWS.Cells(xnRow, 29).value), Nothing, Date.FromOADate(xoExWS.Cells(xnRow, 29).value)) ' xdLeaveStoreTime
                                    xoCmd.Parameters.Add("SFCountLength", SqlDbType.BigInt).Value = xdSFCountLength
                                    xoCmd.Parameters.Add("LastXmitTime", SqlDbType.DateTime).Value = xdLastXmitTime
                                    xoCmd.Parameters.Add("DeltaLastXmitAndSFFinish", SqlDbType.BigInt).Value = xdDeltaLastXmitAndSFFinish
                                    xoCmd.Parameters.Add("TotalTimeInStore", SqlDbType.BigInt).Value = xdTotalTimeInStore
                                    xoCmd.Parameters.Add("EstimatesValue", SqlDbType.Decimal).Value = xnEstimatesValue
                                    xoCmd.Parameters.Add("ActualValue", SqlDbType.Decimal).Value = xnActualValue
                                    xoCmd.Parameters.Add("Variance", SqlDbType.Decimal).Value = xnVariance
                                    xoCmd.Parameters.Add("VariancePcnt", SqlDbType.Decimal).Value = xnVariancePcnt
                                    xoCmd.Parameters.Add("PcntOfBackRoom", SqlDbType.Decimal).Value = xnPcntOfBackRoom
                                    xoCmd.Parameters.Add("BackRoomPrepGrade", SqlDbType.Int).Value = xnBackRoomPrepGrade
                                    xoCmd.Parameters.Add("SalesFloorPrepGuide", SqlDbType.Int).Value = xnSalesFloorPrepGuide
                                    xoCmd.Parameters.Add("StoreManagerName", SqlDbType.NVarChar).Value = xsStoreManagerName
                                    xoCmd.Parameters.Add("RGISSupervisorName", SqlDbType.NVarChar).Value = xsRGISSupervisorName
                                    xoCmd.Parameters.Add("StoreWalkCompleted", SqlDbType.Bit).Value = xbStoreWalkCompleted
                                    xoCmd.Parameters.Add("DidStoreRcvFullSetVarianceRpts", SqlDbType.Bit).Value = xbDidStoreRcvFullSetVarianceRpts
                                    xoCmd.Parameters.Add("EnvLeftWithManager", SqlDbType.Bit).Value = xbEnvLeftWithManager

                                Catch ex As Exception
                                    xsRtnMsg += "Problem when building the s-p M+C parms for Row " & xnRow & " in sheet " & xsWSName & ". Error " & ex.Message & " "
                                End Try


                                '   -------------------------------------------------------------------------------------------------------------------------------------------------
                                ' Case Is = "C STORE"
                                '   Process C Store


                                '   -------------------------------------------------------------------------------------------------------------------------------------------------
                            Case Is = "PFS"
                                '   Process PFS

                                Try
                                    xbProcessThisSheet = True
                                    xnCol = 1
                                    Dim xdCountDate As DateTime? = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 2
                                    Dim xnDist As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 3
                                    Dim xnStoreNumber As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 4
                                    Dim xsLocation As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 5
                                    Dim xnZone As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 6
                                    Dim xnRegion As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 7
                                    Dim xnPcntAccuracy As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 8
                                    Dim xnNoOfAreasChecked As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 9
                                    Dim xnPcntOfAreasChecked As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 10
                                    Dim xnNoOfAuditors As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 11
                                    Dim xdSalesfloorStartTime As DateTime? = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, CombineDateAndTime(xdCountDate, xoExWS.Cells(xnRow, xnCol).value, Nothing))
                                    xnCol = 12
                                    Dim xdSalesfloorEndTime As DateTime? = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, CombineDateAndTime(xdCountDate, xoExWS.Cells(xnRow, xnCol).value, xdSalesfloorStartTime))
                                    xnCol = 13
                                    Dim xdLastXmitTime As DateTime? = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, CombineDateAndTime(xdCountDate, xoExWS.Cells(xnRow, xnCol).value, xdSalesfloorStartTime))
                                    xnCol = 14
                                    Dim xdInventoryLength As Long = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, ConvertDecimalFractionToLong(xoExWS.Cells(xnRow, xnCol).value))
                                    xnCol = 15
                                    Dim xnEstimatesHistoric As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 16
                                    Dim xnActualValue As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 17
                                    Dim xnVariance As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 18
                                    Dim xnVariancePcnt As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 19
                                    Dim xnTotalBackroomValue As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 20
                                    Dim xnTotalSalesFloorValue As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 21
                                    Dim xsSupervisorName As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 22
                                    Dim xsStoreManagerName As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)

                                    xoCmd.CommandText = "p_Stage_DailyReport_PFS_Insert"

                                    xoCmd.Parameters.Add("CountDate", SqlDbType.DateTime).Value = xdCountDate
                                    xoCmd.Parameters.Add("Dist", SqlDbType.Int).Value = xnDist
                                    xoCmd.Parameters.Add("StoreNumber", SqlDbType.Int).Value = xnStoreNumber
                                    xoCmd.Parameters.Add("Location", SqlDbType.NVarChar).Value = xsLocation
                                    xoCmd.Parameters.Add("Zone", SqlDbType.Int).Value = xnZone
                                    xoCmd.Parameters.Add("Region", SqlDbType.Int).Value = xnRegion
                                    xoCmd.Parameters.Add("PcntAccuracy", SqlDbType.Decimal).Value = xnPcntAccuracy
                                    xoCmd.Parameters.Add("NoOfAreasChecked", SqlDbType.Int).Value = xnNoOfAreasChecked
                                    xoCmd.Parameters.Add("PcntOfAreasChecked", SqlDbType.Decimal).Value = xnPcntOfAreasChecked
                                    xoCmd.Parameters.Add("NoOfAuditors", SqlDbType.Int).Value = xnNoOfAuditors
                                    xoCmd.Parameters.Add("SalesfloorStartTime", SqlDbType.DateTime).Value = xdSalesfloorStartTime
                                    xoCmd.Parameters.Add("SalesfloorEndTime", SqlDbType.DateTime).Value = xdSalesfloorEndTime
                                    xoCmd.Parameters.Add("LastXmitTime", SqlDbType.DateTime).Value = xdLastXmitTime
                                    xoCmd.Parameters.Add("InventoryLength", SqlDbType.BigInt).Value = xdInventoryLength
                                    xoCmd.Parameters.Add("EstimatesHistoric", SqlDbType.Decimal).Value = xnEstimatesHistoric
                                    xoCmd.Parameters.Add("ActualValue", SqlDbType.Decimal).Value = xnActualValue
                                    xoCmd.Parameters.Add("Variance", SqlDbType.Decimal).Value = xnVariance
                                    xoCmd.Parameters.Add("VariancePcnt", SqlDbType.Decimal).Value = xnVariancePcnt
                                    xoCmd.Parameters.Add("TotalBackroomValue", SqlDbType.Decimal).Value = xnTotalBackroomValue
                                    xoCmd.Parameters.Add("TotalSalesFloorValue", SqlDbType.Decimal).Value = xnTotalSalesFloorValue
                                    xoCmd.Parameters.Add("SupervisorName", SqlDbType.NVarChar).Value = xsSupervisorName
                                    xoCmd.Parameters.Add("StoreManagerName", SqlDbType.NVarChar).Value = xsStoreManagerName

                                Catch ex As Exception
                                    xsRtnMsg += "Problem when building the s-p Conv parms for Row " & xnRow & " in sheet " & xsWSName & ". Error " & ex.Message & " "
                                End Try


                                '   -------------------------------------------------------------------------------------------------------------------------------------------------
                            Case Is = "PIQA"
                                '   Process PIQA
                                Try
                                    xbProcessThisSheet = True
                                    xnCol = 1
                                    Dim xdInventoryDate As DateTime? = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 2
                                    Dim xnDist As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 3
                                    Dim xnStoreNumber As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 4
                                    Dim xsStoreType As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 5
                                    Dim xsLocation As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 6
                                    Dim xnZone As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 7
                                    Dim xnRegion As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 8
                                    Dim xsStoreContact As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 9
                                    Dim xnAccuracy As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 10
                                    Dim xnAdheranceToProcedure As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 11
                                    Dim xnEfficiency As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 12
                                    Dim xnCourtesy As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 13
                                    Dim xnAppearance As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 14
                                    Dim xnRGISSupervisor As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 15
                                    Dim xnRGISOverall As Integer = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 16
                                    Dim xnPIQAAvge As Decimal = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)
                                    xnCol = 17
                                    Dim xsComments As String = IIf(IsNothing(xoExWS.Cells(xnRow, xnCol).value), Nothing, xoExWS.Cells(xnRow, xnCol).value)

                                    xoCmd.CommandText = "p_Stage_DailyReport_PIQA_Insert"

                                    xoCmd.Parameters.Add("InventoryDate", SqlDbType.DateTime).Value = xdInventoryDate
                                    xoCmd.Parameters.Add("Dist", SqlDbType.Int).Value = xnDist
                                    xoCmd.Parameters.Add("StoreNumber", SqlDbType.Int).Value = xnStoreNumber
                                    xoCmd.Parameters.Add("StoreType", SqlDbType.NVarChar).Value = xsStoreType
                                    xoCmd.Parameters.Add("Location", SqlDbType.NVarChar).Value = xsLocation
                                    xoCmd.Parameters.Add("Zone", SqlDbType.Int).Value = xnZone
                                    xoCmd.Parameters.Add("Region", SqlDbType.Int).Value = xnRegion
                                    xoCmd.Parameters.Add("StoreContact", SqlDbType.NVarChar).Value = xsStoreContact
                                    xoCmd.Parameters.Add("Accuracy", SqlDbType.Int).Value = xnAccuracy
                                    xoCmd.Parameters.Add("AdheranceToProcedure", SqlDbType.Int).Value = xnAdheranceToProcedure
                                    xoCmd.Parameters.Add("Efficiency", SqlDbType.Int).Value = xnEfficiency
                                    xoCmd.Parameters.Add("Courtesy", SqlDbType.Int).Value = xnCourtesy
                                    xoCmd.Parameters.Add("Appearance", SqlDbType.Int).Value = xnAppearance
                                    xoCmd.Parameters.Add("RGISSupervisor", SqlDbType.Int).Value = xnRGISSupervisor
                                    xoCmd.Parameters.Add("RGISOverall", SqlDbType.Int).Value = xnRGISOverall
                                    xoCmd.Parameters.Add("PIQAAvge", SqlDbType.Decimal).Value = xnPIQAAvge
                                    xoCmd.Parameters.Add("Comments", SqlDbType.NVarChar).Value = xsComments

                                Catch ex As Exception
                                    xsRtnMsg += "Problem when building the s-p PIQA parms for Row " & xnRow & " in sheet " & xsWSName & ". Error " & ex.Message & " "
                                End Try

                            Case Else
                                'MessageBox.Show("Unexpected worksheet found: " & xsWSName)
                                Exit For
                        End Select

                        If xbProcessThisSheet Then
                            xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output
                            xoCmd.ExecuteNonQuery()

                            Dim xsWrk As String = xoCmd.Parameters("RowsUpdated").Value.ToString
                            If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then
                                Dim xnWrk2 As Integer = CInt(xsWrk)
                                If xnWrk2 <> 1 Then
                                    xsRtnMsg += "Problem during p_Stage_DailyReport_Insert. "
                                    xbValid = False
                                End If
                            End If
                        End If

                    Catch ex As Exception
                        xsRtnMsg += "Problem when updating data for Row " & xnRow & " in sheet " & xsWSName & ". Error " & ex.Message & " "
                        MessageBox.Show("Error: " & xsRtnMsg)
                    End Try

                    xnRow += 1
                    xnCol = 1
                Loop

            Next

            '   The assumption is that if we've got this far, then we're OK to move the file!
            xbOKToProcess = True

            txtOutput.Text += "Done." & vbCrLf

            MessageBox.Show("Done")

        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message & ": " & xsRtnMsg)
        Finally
            If Not IsNothing(DBConn) AndAlso DBConn.State <> ConnectionState.Closed Then
                DBConn.Close()
            End If

            If Not IsNothing(xoExWB) Then
                xoExWB.Close()
                xoExcel.Quit()
                xoExWS = Nothing
                xoExWB = Nothing
                xoExcel = Nothing
            End If

            '   Move file to Processed folder
            If xbOKToProcess Then
                File.Move(xsIPFileName, xsOPFileName)
            End If
            StatusBarText = "Done"
        End Try

    End Sub

    Private Function CombineDateAndTime(ByVal dDate As Date?, ByVal sTime As String, ByVal dFromDate As Date?) As Date?

        Dim xdRtn As Date? = Nothing

        ' Combine two separate fields into a single date/time field. If either inputs are blank a blank is returned.
        '   If the From date is give, then it means that the inbound time is related to a previous one. It is possible that the new time
        '   is on another day so we need to work out what day to use. The assumption is that a count won't take more than 24 hrs.
        If Not IsNothing(dDate) AndAlso Not IsDBNull(dDate) AndAlso Not IsDBNull(sTime) AndAlso Not IsNothing(sTime) Then
            Dim xdDate As Date
            Dim xdTime As Date
            Dim xdDateFrom As Date
            Dim xdDateTo As Date
            xdDate = dDate

            Dim xnWrk As Integer = sTime.IndexOf(":")
            If xnWrk > 0 Then
                xdTime = CType(sTime, Date)
                Dim xnWrk2 As Decimal
                Dim xnWrk3 As Decimal
                xnWrk2 = xdTime.ToOADate
                xnWrk3 = xdDate.ToOADate + xnWrk2
                xdDate = Date.FromOADate(xnWrk3)
                xdRtn = xdDate
            Else
                Dim xnWrk4 As Decimal
                Dim xnWrk5 As Decimal
                xnWrk4 = CType(sTime, Decimal)
                xnWrk5 = xdDate.ToOADate + xnWrk4
                xdDate = Date.FromOADate(xnWrk5)
                xdRtn = xdDate
            End If

            If Not IsNothing(dFromDate) Then
                '   If the new date is prior to the FromDate then we need to increment the day
                xdDateFrom = dFromDate
                xdDateTo = xdRtn
                'xdDateOut
                If DateDiff(DateInterval.Minute, xdDateFrom, xdDateTo) < 0 Then
                    xdDateTo = DateAdd(DateInterval.Day, 1, xdDateTo)
                    xdRtn = xdDateTo
                End If
            End If

        End If

        Return xdRtn
    End Function

    Private Function ImportToTimeSpan(sTime As String) As TimeSpan?

        Dim xoRtn As TimeSpan? = Nothing

        If sTime.Length > 0 Then

            Dim xnWrk As Integer = sTime.IndexOf(":")
            Dim xoTS As TimeSpan
            Dim xdTime As DateTime
            If xnWrk > 0 Then
                xdTime = CType(sTime, Date)
            Else
                Dim xnWrk4 As Decimal = CType(sTime, Decimal)
                xdTime = Date.FromOADate(xnWrk4)
            End If
            xoTS = New TimeSpan(xdTime.Hour, xdTime.Minute, xdTime.Second)
            xoRtn = xoTS

        End If

        Return xoRtn
    End Function

    Private Function ConvertDurnToLong(oTimeSpan As TimeSpan) As Long?

        Dim xnRtn As Long = Nothing

        If oTimeSpan.Ticks > 0 Then
            Dim xnWrk As Long = oTimeSpan.Ticks
        End If

        Return xnRtn
    End Function


    Private Function ConvertDurnToLong(sTime As String) As Long?

        Dim xnRtn As Long = Nothing

        Dim xoTS As TimeSpan = ImportToTimeSpan(sTime)

        If xoTS.Ticks > 0 Then
            Dim xnWrk As Long = xoTS.Ticks
        End If

        Return xnRtn
    End Function

    Private Function ConvertDurnToLong(nTime As Double) As Long?

        Dim xnRtn As Long = Nothing

        Dim xoTS As New TimeSpan(nTime)

        If nTime <> 0 Then
            '   Get the decimal portion and recast as a long
            Dim xnWrkI As Long = Math.Truncate(nTime)
            Dim xnWrkd As Decimal = nTime - xnWrkI

            Dim xnWrk As Long = xoTS.Ticks
        End If

        Return xnRtn
    End Function

    Private Function ConvertDecimalFractionToLong(nNumber As Decimal) As Long

        Dim xnRtn As Long = 0
        Dim xsWrkIn As String = ""
        Dim xsWrkOut As String = ""

        If nNumber <> 0 Then
            Dim xnWrkI As Long = Math.Truncate(nNumber)
            Dim xnWrkD As Decimal = nNumber - xnWrkI
            xsWrkIn = CType(xnWrkD, String)
            For xnCount As Integer = 2 To xsWrkIn.Length - 1
                xsWrkOut += xsWrkIn.Substring(xnCount, 1)
            Next
            If xsWrkOut.Length > 0 Then
                Dim xdDate As Date = Date.FromOADate(nNumber)
                Dim xnHH As Integer = xdDate.Hour
                Dim xnMM As Integer = xdDate.Minute
                Dim xnSS As Integer = xdDate.Second
                Dim xoTS As New TimeSpan(xnHH, xnMM, xnSS)
                'xnRtn = CType(xsWrkOut, Long) '* DateTime.ticksperday
                xnRtn = xoTS.Ticks
            End If
        End If

        Return xnRtn
    End Function


    Private Sub btnSupercatVerify_Click(sender As System.Object, e As System.EventArgs) Handles btnSupercatVerify.Click

        '   Get input data/Open the file
        '   Get the file name
        Dim xsRtnMsg As String = ""
        Dim xbFirstDataRowFound As Boolean

        Try


            txtOutput.Text = "Import Supercat verifier:" & vbCrLf
            StatusBarText = "Running Supercat verifier"

            '            txtFileNameAndPathSuperCat.Text = "C:\_WIP\MarchWeb\Clients\RGIS\SuperCatVerifier data\SuperCat_Verifier_0003.out36"
            txtFileNameAndPathSuperCat.Text = "C:\_WIP\MarchWeb\Clients\RGIS\SuperCatVerifier data\SuperCat_Verifier_0003.out36"

            '   Identify the file to import
            dlgFileOpen.FileName = txtFileNameAndPathSuperCat.Text
            dlgFileOpen.ShowDialog()
            txtFileNameAndPathSuperCat.Text = dlgFileOpen.FileName

        

        Catch ex As Exception
            MessageBox.Show("Problem getting the input file2 . Job cancelled.")
            Return
        End Try

        MessageBox.Show("Done")
        StatusBarText = ""

    End Sub

    Private Sub ProcessSuperCatVarifierData()

        '   Get input data/Open the file
        '   Get the file name
        Dim xsRtnMsg As String = ""
        Dim xbFirstDataRowFound As Boolean

        Try

            If Not File.Exists(txtFileNameAndPathSuperCat.Text) Then
                MessageBox.Show("File name not found. Job cancelled.")
                Return
            End If
        Catch ex As Exception
            MessageBox.Show("Problem getting the input file3 . Job cancelled.")
            Return
        End Try

        '   Delete the output file then load the object ready to write the rows
        If File.Exists(txtOPFileName.Text) Then
            Kill(txtOPFileName.Text)
        End If

        Dim xoSR As New StreamReader(txtFileNameAndPathSuperCat.Text)

        'Dim xsClientName As String
        'If radDev.Checked Then
        '    xsClientName = "SSDev"
        'Else
        '    xsClientName = "SS"
        'End If

        Dim xbValid As Boolean = False
        Dim xoLic = New RGISLic.LicVal

        'If xoLic.GetDBForClient(xsClientName, "Data Source=smartspacedatacenter.com\BOE140;Initial Catalog=RGIS_Dashboard; Integrated Security=False; User ID=sa; Password=CjC7vHYB2yESaBgq") Then
        '    xbValid = True
        'Else
        '    '   If bad, then exit
        '    MessageBox.Show("Error getting the DB details: " & xoLic.RtnMsg)
        '    Exit Sub
        'End If

        StatusBarText = "Setting up the expected columns and clearing existing data."

        'Dim xsConnStr As String = "Data Source=" & xoLic.DBLocn & ";Initial Catalog=" & xoLic.DBName & "; Integrated Security=False; User ID=sa; Password=CjC7vHYB2yESaBgq"

        'Dim xsConnStr As String = "Server=CND42412H2\STPL_08;Initial Catalog=Sainsburys;Integrated Security=SSPI;"        '   Clear the work tables
        xbValid = True
        DBConn = New SqlConnection(mConn)
        Dim xoCmd As New SqlCommand
        DBConn.Open()

        '   Open the file as a stream
        Try

            Dim xbStoreFound As Boolean = False
            Dim xbHeadingFound As Boolean = False
            Dim xnWrk As Integer = 0
            Dim xsWrk As String = ""
            Dim xnRowCount As Integer = 0
            Dim xnStore As Integer = 0
            Dim xnZone As Integer = 0
            Dim xnRegion As Integer = 0
            Dim xsEstateType As String = ""
            Dim xsStoreName As String = ""
            Dim xsSuperCatName As String = ""
            Dim xsCurrentCountYear As String = ""
            Dim xsCurrentPeriod As String = ""
            Dim xbClothing As Boolean = False
            Dim xdReportDate As Date
            Dim xbIsTA As Boolean = False

            '   The column splits prior to 5th Dec 2014 were
            '   1-40, 43-51, 53-66, 70-82, 86-99, 102-115, 118-131
            '   Post are 

            Dim xoRptCols As New RptCols
            xoRptCols.RptColsIP = New Collection
            Dim xoRptCol As New RptCol

            xoRptCol.ColHdgInput = "Super Cat Name"
            xoRptCol.ColName = "Super Cat Name"
            xoRptCol.ColStartInput = 0
            xoRptCol.ColEndInput = 40
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTString
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "Super Cat"
            xoRptCol.ColName = "Super Cat"
            xoRptCol.ColStartInput = 42
            xoRptCol.ColEndInput = 50
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTInteger
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "Unique # SKUs Counted"
            xoRptCol.ColName = "SKUs Counted"
            xoRptCol.ColStartInput = 52
            xoRptCol.ColEndInput = 64
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTInteger
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "Unique # SKUs Checked in BR"
            xoRptCol.ColName = "SKUs Checked BR"
            xoRptCol.ColStartInput = 66
            xoRptCol.ColEndInput = 79
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTInteger
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "Unique # SKUs Checked in SF"
            xoRptCol.ColName = "SKUs Checked SF"
            xoRptCol.ColStartInput = 81
            xoRptCol.ColEndInput = 94
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTInteger
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "% SKUs Checked"
            xoRptCol.ColName = "SKUs Checked Pcnt"
            xoRptCol.ColStartInput = 96
            xoRptCol.ColEndInput = 104
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTDecimal
            xoRptCol.ColDecimals = 2
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "# of Ammendment Made"
            xoRptCol.ColName = "Changes made"
            xoRptCol.ColStartInput = 108
            xoRptCol.ColEndInput = 121
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTInteger
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "Accuracy %"
            xoRptCol.ColName = "Accuracy Pcnt"
            xoRptCol.ColStartInput = 123
            xoRptCol.ColEndInput = 132
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTDecimal
            xoRptCol.ColDecimals = 2
            xoRptCols.RptColsIP.Add(xoRptCol)

            ' S:\SuperCatVerifier\ORRIDGE - SuperCat_Verifier_148.out36

            Dim xsLine As String = xoSR.ReadLine()
            Do While Not xoSR.EndOfStream

                xnRowCount += 1

                '   Get the Store ID
                If Not xbStoreFound Then
                    If xnRowCount > 12 Then
                        MessageBox.Show("Store not found in 1st 12 rows. Probable data format issue. Job cancelled")
                        Exit Sub
                    End If

                    xsWrk = xsLine.Substring(0, 20).Trim
                    If xsWrk.Length > 0 Then
                        xnWrk = xsWrk.IndexOf("Store:")
                        If xnWrk > -1 Then
                            xsWrk = xsWrk.Substring(xnWrk + 7)

                            'JR - 19/09/2017
                            'Remove any leading space(s) before store number
                            xsWrk = LTrim(xsWrk)

                            ' JR - 14/03/2017
                            ' Remove any leading comma before the store number to prevent a conversion error
                            If xsWrk.Contains(",") Then
                                xsWrk = xsWrk.Replace(",", "")
                            End If
 
                            Dim xnEnd As Integer = xsWrk.IndexOf(" ")

                            If xnEnd > -1 Then
                                xsWrk = xsWrk.Substring(0, xnEnd)
                                xnStore = CType(xsWrk, Integer)

                                '   Get the report date
                                xsWrk = xsLine.Substring(108, 14)
                                xdReportDate = CDate(xsWrk)

                                '   Get the Period/Count Year for the date
                                'p_GetPeriodAndCYForDate


                                'xsCurrentCountYear = "CY14"     '   possibly derived from report date and FinCal tbl

                                'Dim xsSQL As String = ""
                                'Dim xoDS As New DataSet

                                'xoCmd = New SqlCommand
                                'xoCmd.Connection = DBConn

                                'xsSQL = "SELECT [Count_Year] FROM [Financial_Calender] WHERE [Inventory Date]=CONVERT(VARCHAR(10),'" & xdReportDate & "',111)"
                                'xoCmd.CommandType = CommandType.Text

                                'Try

                                '    Dim xoDA As New SqlDataAdapter(xsSQL, DBConn)
                                '    xoDS = New DataSet
                                '    xoDA.Fill(xoDS)

                                '    If Not IsNothing(xoDS) AndAlso xoDS.Tables(0).Rows.Count > 0 Then
                                '        xsCurrentCountYear = xoDS.Tables(0).Rows(0).Item("Count_Year")
                                '    End If

                                'Catch ex As Exception
                                '    xsRtnMsg += "Error during setting the current year: " & ex.Message
                                'End Try

                                xoCmd = New SqlCommand
                                xoCmd.Connection = DBConn
                                xoCmd.CommandType = CommandType.StoredProcedure

                                Try
                                    xoCmd.CommandText = "p_GetPeriodAndCYForDate"
                                    xoCmd.Parameters.Add("InputDate", SqlDbType.Date).Value = xdReportDate
                                    xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
                                    xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
                                    xoCmd.ExecuteNonQuery()

                                    xsCurrentCountYear = ""
                                    xsWrk = xoCmd.Parameters("Count_Year").Value.ToString
                                    If xsWrk.Length > 0 Then
                                        xsCurrentCountYear = xsWrk
                                    End If

                                    xsCurrentPeriod = ""
                                    xsWrk = xoCmd.Parameters("Period_No").Value.ToString
                                    If xsWrk.Length > 0 Then
                                        xsCurrentPeriod = xsWrk
                                    End If

                                    '   Clear the rows (if any) that are in the table
                                    xoCmd = New SqlCommand
                                    xoCmd.Connection = DBConn
                                    xoCmd.CommandType = CommandType.StoredProcedure

                                    Try
                                        xoCmd.CommandText = "p_SuperCatVerifier_DeleteCountYearStore"
                                        xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xsCurrentCountYear
                                        xoCmd.Parameters.Add("Store_Number", SqlDbType.Int).Value = xnStore
                                        xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output
                                        xoCmd.ExecuteNonQuery()

                                    Catch ex As Exception
                                        xsRtnMsg += "Problem when clearing TB_SuperCatVerifier. Error " & ex.Message & " "
                                    End Try


                                Catch ex As Exception
                                    xsRtnMsg += "Problem when processing header data. Error " & ex.Message & " "
                                End Try

                                xoCmd = New SqlCommand
                                xoCmd.Connection = DBConn
                                xoCmd.CommandType = CommandType.StoredProcedure

                                Try
                                    xoCmd.CommandText = "p_GetZoneRegionForStore"
                                    xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xsCurrentCountYear
                                    xoCmd.Parameters.Add("Store_Number", SqlDbType.Int).Value = xnStore
                                    xoCmd.Parameters.Add("Estate_Type", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output
                                    xoCmd.Parameters.Add("Zone", SqlDbType.Int).Direction = ParameterDirection.Output
                                    xoCmd.Parameters.Add("Region", SqlDbType.Int).Direction = ParameterDirection.Output
                                    xoCmd.Parameters.Add("Store_Name", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output
                                    xoCmd.ExecuteNonQuery()

                                    xsEstateType = xoCmd.Parameters("Estate_Type").Value.ToString
                                    If xsWrk.Length = 0 Then
                                        xsRtnMsg += "Problem during p_GetZoneRegionForStore: Missing Store type for " & xnStore & ". "
                                    End If

                                    xnZone = 0
                                    xsWrk = xoCmd.Parameters("Zone").Value.ToString
                                    If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then
                                        xnZone = CInt(xsWrk)
                                        If xnZone <= 0 Then
                                            xsRtnMsg += "Problem during p_GetZoneRegionForStore: Missing Zone for " & xnStore & ". "
                                            xbValid = False
                                        End If
                                    End If

                                    xnRegion = 0
                                    xsWrk = xoCmd.Parameters("Region").Value.ToString
                                    If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then
                                        xnRegion = CInt(xsWrk)
                                        If xnRegion <= 0 Then
                                            xsRtnMsg += "Problem during p_GetZoneRegionForStore: Missing Region for " & xnStore & ". "
                                            xbValid = False
                                        End If
                                    End If

                                    xsStoreName = ""
                                    xsWrk = xoCmd.Parameters("Store_Name").Value.ToString
                                    If xsWrk.Length > 0 Then
                                        xsStoreName = xsWrk
                                    End If





                                    xoCmd = New SqlCommand
                                    xoCmd.Connection = DBConn
                                    xoCmd.CommandType = CommandType.StoredProcedure

                                    Try
                                        xoCmd.CommandText = "p_IsTransactionTA"
                                        xoCmd.Parameters.Add("Store_Number", SqlDbType.Int).Value = xnStore
                                        xoCmd.Parameters.Add("InputDate", SqlDbType.Date).Value = xdReportDate
                                        xoCmd.Parameters.Add("IsTA", SqlDbType.NVarChar, 5).Direction = ParameterDirection.Output
                                        xoCmd.ExecuteNonQuery()

                                        xbIsTA = False
                                        xsWrk = xoCmd.Parameters("IsTA").Value.ToString
                                        If xsWrk.Length > 0 Then
                                            xbIsTA = CType(xsWrk, Boolean)
                                        End If

                                        '   Clear the rows (if any) that are in the table
                                        xoCmd = New SqlCommand
                                        xoCmd.Connection = DBConn
                                        xoCmd.CommandType = CommandType.StoredProcedure

                                    Catch ex As Exception
                                        xsRtnMsg += "Problem when detecting TA date. Error " & ex.Message & " "
                                    End Try




                                    '   Clear the rows (if any) that are in the table
                                    xoCmd = New SqlCommand
                                    xoCmd.Connection = DBConn
                                    xoCmd.CommandType = CommandType.StoredProcedure

                                    Try
                                        xoCmd.CommandText = "p_SuperCatVerifier_DeleteCountYearStore"
                                        xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xsCurrentCountYear
                                        xoCmd.Parameters.Add("Store_Number", SqlDbType.Int).Value = xnStore
                                        xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output
                                        xoCmd.ExecuteNonQuery()

                                    Catch ex As Exception
                                        xsRtnMsg += "Problem when clearing TB_SuperCatVerifier. Error " & ex.Message & " "
                                    End Try


                                Catch ex As Exception
                                    xsRtnMsg += "Problem when processing header data. Error " & ex.Message & " "
                                End Try

                                xbClothing = "False"

                                xbStoreFound = True

                            End If

                        End If

                    End If

                Else

                    '   Ignore blank rows (blank between chars 1-20)
                    xsWrk = xsLine.Substring(0, 20).Trim
                    If xsWrk.Length > 0 Then

                        If Not xbHeadingFound Then
                            xnWrk = xsWrk.IndexOf("------", 1)
                            If xnWrk > 0 Then
                                xbHeadingFound = True
                            End If

                        Else

                            '   Dont process a blank line or the Grand Total line
                            xsWrk = xsLine.Substring(0, 20).Trim
                            If xsWrk.Length > 0 And xsLine.IndexOf("Grand Total") = -1 Then

                                If Not xbFirstDataRowFound Then
                                    StatusBarText = "Processing report rows."
                                    xbFirstDataRowFound = True
                                End If

                                '   Confirm data format
                                Dim xnColCount As Integer = -1
                                Dim xnSuperCatID As Integer = 0
                                Dim xnSKUsCounted As Integer = 0
                                Dim xnSKUsCheckedPcnt As Decimal = 0
                                Dim xnSKUsCheckedBR As Decimal = 0
                                Dim xnSKUsCheckedSF As Decimal = 0
                                Dim xnSKUsCheckedcalc As Integer = 0
                                Dim xnChangesMade As Integer = 0
                                Dim xnAccuracy As Decimal = 0

                                '   Load data columns

                                For Each xoCol As RptCol In xoRptCols.RptColsIP
                                    xsWrk = xsLine.Substring(xoCol.ColStartInput, xoCol.ColEndInput - xoCol.ColStartInput).Trim
                                    xoCol.ColStringValue = xsWrk
                                    '        MsgBox(xoCol.ColName)
                                    xnColCount += 1
                                    Select Case xoCol.ColName
                                        Case Is = "Super Cat Name"
                                            xsSuperCatName = xoCol.ColStringValue

                                        Case Is = "Super Cat"
                                            If xsWrk.Length > 0 Then
                                                xnSuperCatID = CType(xsWrk, Integer)

                                                xoCmd = New SqlCommand
                                                xoCmd.Connection = DBConn
                                                xoCmd.CommandType = CommandType.StoredProcedure

                                                xoCmd.CommandText = "p_IsSuperCatInClothing"
                                                xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xsCurrentCountYear
                                                xoCmd.Parameters.Add("Super_Category", SqlDbType.Int).Value = xnSuperCatID
                                                xoCmd.Parameters.Add("Clothing", SqlDbType.Bit).Direction = ParameterDirection.Output
                                                xoCmd.Parameters.Add("Super_Category_Name", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output
                                                xoCmd.ExecuteNonQuery()

                                                xsWrk = xoCmd.Parameters("Clothing").Value.ToString
                                                xbClothing = False
                                                If xsWrk.Length > 0 Then
                                                    If xsWrk = "1" Or xsWrk = "True" Then
                                                        xbClothing = True
                                                    End If
                                                End If

                                                xsWrk = xoCmd.Parameters("Super_Category_Name").Value.ToString
                                                xsSuperCatName = ""
                                                If xsWrk.Length > 0 Then
                                                    xsSuperCatName = xsWrk
                                                End If
                                            End If

                                        Case Is = "SKUs Counted"
                                            If xsWrk.Length > 0 Then
                                                xnSKUsCounted = CType(xsWrk, Integer)
                                            End If

                                        Case Is = "SKUs Checked BR"
                                            If xsWrk.Length > 0 Then
                                                xnSKUsCheckedBR = CType(xsWrk, Integer)
                                            End If

                                        Case Is = "SKUs Checked SF"
                                            If xsWrk.Length > 0 Then
                                                xnSKUsCheckedSF = CType(xsWrk, Integer)
                                            End If

                                        Case Is = "SKUs Checked Pcnt"
                                            If xsWrk.Length > 0 Then
                                                xnSKUsCheckedPcnt = CType(xsWrk, Decimal)
                                            End If

                                        Case Is = "Changes made"
                                            If xsWrk.Length > 0 Then
                                                xnChangesMade = CType(xsWrk, Integer)
                                            End If

                                        Case Is = "Accuracy Pcnt"
                                            If xsWrk.Length > 0 Then
                                                xnAccuracy = CType(xsWrk, Decimal)
                                            End If
                                    End Select
                                Next

                                'xoRptCols.RptColsIP(0).Item(0).ToString()


                                '   Load the data row

                                '   Write to the SuperCatVerifier table
                                xoCmd = New SqlCommand
                                xoCmd.Connection = DBConn
                                xoCmd.CommandType = CommandType.StoredProcedure

                                Try
                                    xoCmd.CommandText = "p_SuperCatVerifier_Insert"
                                    xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xsCurrentCountYear
                                    xoCmd.Parameters.Add("Estate_Type", SqlDbType.NVarChar, 50).Value = xsEstateType
                                    xoCmd.Parameters.Add("Zone", SqlDbType.Int).Value = xnZone
                                    xoCmd.Parameters.Add("Region", SqlDbType.Int).Value = xnRegion
                                    xoCmd.Parameters.Add("Store_Number", SqlDbType.Int).Value = xnStore
                                    xoCmd.Parameters.Add("Store_Name", SqlDbType.NVarChar, 50).Value = xsStoreName
                                    xoCmd.Parameters.Add("ReportDate", SqlDbType.DateTime).Value = xdReportDate
                                    xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar).Value = xsCurrentPeriod
                                    xoCmd.Parameters.Add("Clothing", SqlDbType.Bit).Value = xbClothing
                                    xoCmd.Parameters.Add("Super_Category", SqlDbType.Int).Value = xnSuperCatID
                                    xoCmd.Parameters.Add("Super_Category_Name", SqlDbType.NVarChar, 50).Value = xsSuperCatName
                                    'Change next line to back calculate skus checked from skus counted/skus checked pcnt remember to trap divzero
                                    '     xoCmd.Parameters.Add("SKUsChecked", SqlDbType.Int).Value = xnSKUsCheckedBR + xnSKUsCheckedSF

                                    If xnSKUsCheckedPcnt = 0 Then
                                        xnSKUsCheckedcalc = 0
                                    Else
                                        xnSKUsCheckedcalc = xnSKUsCounted * (xnSKUsCheckedPcnt / 100)
                                    End If
                                    xoCmd.Parameters.Add("SKUsChecked", SqlDbType.Int).Value = xnSKUsCheckedcalc

                                    xoCmd.Parameters.Add("SKUsCounted", SqlDbType.Int).Value = xnSKUsCounted
                                    xoCmd.Parameters.Add("SKUsCheckedPcnt", SqlDbType.Decimal).Value = xnSKUsCheckedPcnt
                                    xoCmd.Parameters.Add("NoOfChanges", SqlDbType.Int).Value = xnChangesMade
                                    xoCmd.Parameters.Add("AccuracyPcnt", SqlDbType.Decimal).Value = xnAccuracy

                                    xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output
                                    xoCmd.ExecuteNonQuery()

                                    xsWrk = xoCmd.Parameters("RowsUpdated").Value.ToString
                                    If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then

                                        Dim xnWrk2 As Integer = CInt(xsWrk)
                                        If xnWrk2 <> 1 Then
                                            xsRtnMsg += "Problem during p_SuperCatVerifier_Insert. "
                                            xbValid = False
                                        End If
                                    End If

                                Catch ex As Exception
                                    xsRtnMsg += "Problem when updating data. Error " & ex.Message & " "
                                    MessageBox.Show("Error during Insert: Error " & ex.Message)
                                End Try

                            End If
                        End If
                    End If

                End If

                '   When the Grand Total line is encountered, terminate the run
                If xsWrk.Length > 0 And xsLine.IndexOf("Grand Total") >= 0 Then
                    Exit Do
                End If

                xsLine = xoSR.ReadLine()

            Loop

        Catch ex As Exception
            MessageBox.Show("Problem getting the excel file . Job cancelled. " & ex.Message & " " & txtFileNameAndPathSuperCat.Text)
            Return
        Finally

            If Not IsNothing(DBConn) Then
                DBConn.Close()
                xoCmd = Nothing
                DBConn = Nothing
            End If
        End Try


    End Sub


    Private Sub btnClothingImportSales_Click(sender As System.Object, e As System.EventArgs) Handles btnClothingImportSales.Click

        '   Import data into Sansbury's Clothing Staging table

        Dim xnWSCount As Integer = 0
        Dim xsRtnMsg As String = ""

        Dim xadDates() As Date = Nothing
        Dim xanDateCol() As Integer = Nothing
        Dim xasCY() As String = Nothing
        Dim xasPeriod() As String = Nothing
        Dim xanDateValSales() As Decimal = Nothing
        Dim xasDateCodeCol() As String = Nothing
        'Dim xanDateValExposure() As Decimal = Nothing
        Dim xanDateValClExpAdj() As Decimal = Nothing
        Dim xanDateValRGISExpAdj() As Decimal = Nothing

        Try
            Cursor = Cursors.WaitCursor
            txtOutput.Text = "Output" & vbCrLf

            '   Identify the file to import
            dlgFileOpen.FileName = txtFileNameAndPathIPClothing.Text
            dlgFileOpen.ShowDialog()
            txtFileNameAndPathIPClothing.Text = dlgFileOpen.FileName

            If txtFileNameAndPathIPClothing.Text.Length = 0 Then
                Cursor = Cursors.Default
                MessageBox.Show("Missing file name. Job cancelled.")
                Return
            End If

            If Not File.Exists(txtFileNameAndPathIPClothing.Text) Then
                Cursor = Cursors.Default
                MessageBox.Show("File name not found. Job cancelled.")
                Return
            End If

            '   Get the DB Connection string
            '   Currently this is via the Radio buttons to vary Dev/Live. The client may only be Sainsburys
            'Dim xsClientName As String
            'If radDev.Checked Then
            '    xsClientName = "SSDev"
            'Else
            '    xsClientName = "SS"
            'End If

            Dim xbValid As Boolean = False
            Dim xoLic = New RGISLic.LicVal

            'If xoLic.GetDBForClient(xsClientName, "Data Source=smartspacedatacenter.com\BOE140;Initial Catalog=RGIS_Dashboard; Integrated Security=False; User ID=sa; Password=CjC7vHYB2yESaBgq") Then
            '    xbValid = True
            'Else
            '    '   If bad, then exit
            '    MessageBox.Show("Error getting the DB details: " & xoLic.RtnMsg)
            '    Exit Sub
            'End If

            ''   Build the connection string
            'Dim xsConnStr As String = "Data Source=" & xoLic.DBLocn & ";Initial Catalog=" & xoLic.DBName & "; Integrated Security=False; User ID=sa; Password=CjC7vHYB2yESaBgq"

            'Dim xsConnStr As String = "Server=CND42412H2\STPL_08;Initial Catalog=Sainsburys;Integrated Security=SSPI;"            '   Clear the work tables
            xbValid = True

            DBConn = New SqlConnection(mConn)
            Dim xoCmd As New SqlCommand

            DBConn.Open()


            '   Open the workbook
            Dim xoExcel As Excel.Application
            Dim xoExWS As New Excel.Worksheet
            Dim xoExWB As Excel.Workbook

            StatusBarText = "Opening Excel file. "

            xoExcel = New Excel.Application

            xoExWB = xoExcel.Workbooks.Open(txtFileNameAndPathIPClothing.Text)
            xoExcel.Visible = True
            xoExcel.Application.DisplayAlerts = False

            StatusBarText = "Processing the worksheets."

            ' Jeremy Rogers - 15/08/2017
            ' Changed code so that it only reads data from last sheet in the Workbook
            xoExWS = xoExWB.Worksheets(xoExWB.Worksheets.Count)

            '   Work through each worksheet in the workbook
            '   There will be a number of sheets for Sales and a number for Shrink
            '   The different types have different column layouts as the Shrink has 2 columns per day

            Dim xsWSName As String = xoExWS.Name
            Dim xbDatesFound As Boolean = False
            Dim xnDateCount As Integer = 0

            ReDim xadDates(xnDateCount)
            ReDim xanDateCol(xnDateCount)
            ReDim xasDateCodeCol(xnDateCount)
            ReDim xanDateValSales(xnDateCount)
            'ReDim xanDateValExposure(xnDateCount)
            ReDim xanDateValClExpAdj(xnDateCount)
            ReDim xanDateValRGISExpAdj(xnDateCount)

            '   Determine the sheet type
            Dim xnWrk As Integer = 0
            txtOutput.Text += xoExWS.Name & " " & vbCrLf
            StatusBarText = "Processing worksheet " & xoExWS.Name & "."


            '   Get the date range
            Dim xsDate As String = ""
            Dim xnDateStartCol As Integer = 6
            Dim xnDateStartRow As Integer = 1
            Dim xnDateEndCol As Integer = 0
            Dim xnRow As Integer = 1
            Dim xnCol As Integer = 1
            Dim xnColForThisDate As Integer = 0

            Dim CellData As Object(,) = xoExWS.UsedRange.Value

            '   Find the "Day" row
            '   The word "Day" appears in the cell preceding the row of dates. This can be in any row from 5-10 and any column from E(5) to K(11)
            xnRow = 5
            Dim xbFound As Boolean = False
            For xnRow = 5 To 10
                For xnCol = 5 To 11
                    If Not IsNothing(CellData(xnRow, xnCol)) AndAlso CellData(xnRow, xnCol).ToString.ToUpper = "DAY" Then
                        xbFound = True
                        Exit For
                    End If
                Next
                If xbFound Then Exit For
            Next

            '   Now extract the dates from the cells. There can be 1-3 columns per date with no rhyme or reason as to why (!)
            '   We need to record what date is relevant to each column so that the figures on each row can be totalled for the table update.
            '   The assumption is that all columns for a given date are to be added together and that they appear together (i.e. are not jumbled up in the column order)
            xnDateStartCol = xnCol + 1
            xnDateStartRow = xnRow
            Dim xdsvDate As Date = Nothing
            Dim xdWrk As Date = Nothing
            xnDateCount = -1

            For xnCol = xnDateStartCol To 30

                If IsNothing(CellData(xnRow, xnCol)) Then
                    Exit For
                End If

                xsDate = CellData(xnRow, xnCol).ToString()
                If xsDate.Length = 0 Or Not IsDate(xsDate) Then
                    Exit For
                End If

                xdWrk = CDate(xsDate)
                If IsNothing(xdsvDate) OrElse xdsvDate <> xdWrk Then

                    xnDateCount += 1
                    ReDim Preserve xadDates(xnDateCount)
                    ReDim Preserve xasCY(xnDateCount)
                    ReDim Preserve xasPeriod(xnDateCount)
                    Dim xdDate As Date = CDate(xsDate)
                    xadDates(xnDateCount) = xdDate
                    'p_GetPeriodAndCYForDate

                    xoCmd = New SqlCommand
                    xoCmd.Connection = DBConn
                    xoCmd.CommandType = CommandType.StoredProcedure

                    Try
                        ' Dim xdReportDate As Date
                        xoCmd.CommandText = "p_GetPeriodAndCYForDate"
                        xoCmd.Parameters.Add("InputDate", SqlDbType.Date).Value = xdDate
                        xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
                        xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
                        xoCmd.ExecuteNonQuery()

                        Dim xsCurrentCountYear As String = ""
                        Dim xsWrk As String = xoCmd.Parameters("Count_Year").Value.ToString
                        If xsWrk.Length > 0 Then
                            xsCurrentCountYear = xsWrk
                        End If

                        Dim xsCurrentPeriod As String = ""
                        xsWrk = xoCmd.Parameters("Period_No").Value.ToString
                        If xsWrk.Length > 0 Then
                            xsCurrentPeriod = xsWrk
                        End If

                        xasCY(xnDateCount) = xsCurrentCountYear
                        xasPeriod(xnDateCount) = xsCurrentPeriod

                    Catch ex As Exception
                        xsRtnMsg += "Problem when processing header data. Error " & ex.Message & " "
                    End Try


                    xbDatesFound = True
                    xdsvDate = xdWrk

                End If

                ReDim Preserve xanDateCol(xnCol)
                xanDateCol(xnCol) = xnDateCount


            Next

            xnDateEndCol = xnCol

            If Not xbDatesFound Then
                xsRtnMsg += "No dates found on worksheet " & xsWSName
                txtOutput.Text += "No dates found in " & xsWSName & vbCrLf
                ' Need to break out of function here ?
            End If

            '   Extract the data
            Dim xnRowCount As Integer = xoExWS.UsedRange.Rows.Count
            xnRow = 1
            Do While CellData(xnRow, 1) <> "SSL" And xnRow < xnRowCount
                xnRow += 1
            Loop

            If xnRow >= xnRowCount Then
                txtOutput.Text += "No data found in " & xsWSName & vbCrLf
            End If

            '   First data row found so process all the following non blank rows
            Do While xnRow < xnRowCount And CellData(xnRow, 1).ToString().Length > 0
                Dim xdDate As Date? = Nothing
                Dim xsStore As String = ""
                Dim xnStore As Integer = 0

                'xnRow += 1
                'xsRtnMsg = "a" & xnRow

                xsStore = CellData(xnRow, 4)
                If Not IsNothing(xsStore) And xsStore.Length > 0 Then
                    xnStore = CInt(xsStore)
                    If xnStore > 0 Then
                        '   Get the figure for each day
                        '   Note that (at this time) there will be an output row for each day, even for zero values
                        '   The s-p Inserts if there's no row there or updates if one does exist
                        '   We work through all columns to accumulate date and then output all dates at one time

                        ReDim xanDateValSales(xadDates.GetUpperBound(0))

                        For xnCol = xnDateStartCol To xnDateEndCol - 1

                            Dim xnVal As Decimal = 0
                            Dim xsWrk As String = CellData(xnRow, xnCol)
                            If Not IsNothing(xsWrk) AndAlso xsWrk.Length > 0 Then
                                xnVal = Val(xsWrk)
                            End If

                            xanDateValSales(xanDateCol(xnCol)) += xnVal


                        Next

                        For xnWrk = 0 To xnDateCount '- 1
                            xoCmd = New SqlCommand
                            xoCmd.Connection = DBConn
                            xoCmd.CommandType = CommandType.StoredProcedure

                            Try
                                xoCmd.CommandText = "p_StageClothingInsertOrUpdate"
                                xoCmd.Parameters.Add("MetricDate", SqlDbType.DateTime).Value = xadDates(xnWrk)
                                xoCmd.Parameters.Add("StoreNumber", SqlDbType.Int).Value = xnStore
                                xoCmd.Parameters.Add("TotalSales", SqlDbType.Decimal).Value = xanDateValSales(xnWrk)
                                xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar, 10).Value = xasCY(xnWrk)
                                xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar, 10).Value = xasPeriod(xnWrk)
                                xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output
                                xoCmd.ExecuteNonQuery()

                                Dim xsWrk As String = xoCmd.Parameters("RowsUpdated").Value.ToString
                                If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then
                                    Dim xnWrk2 As Integer = CInt(xsWrk)
                                    If xnWrk2 <> 1 Then
                                        xsRtnMsg += "Problem during p_StageClothingInsert/Update. "
                                        xbValid = False
                                    End If
                                End If
                            Catch ex As Exception
                                xsRtnMsg += "Problem when updating data for row " & xnRow & " in sheet " & xsWSName & ", store " & xnStore & ", date " & xadDates(xnWrk) & _
                                    ", value " & xanDateValSales(xnWrk) & ". Error " & ex.Message & ". "
                            End Try

                            If xsRtnMsg.Length > 0 Then
                                xsRtnMsg += " Job cancelled."
                                Cursor = Cursors.Default
                                MessageBox.Show(xsRtnMsg)
                                txtOutput.Text += xsRtnMsg
                                Exit Sub
                            End If

                        Next

                        'xsRtnMsg += "c"

                    End If
                End If
                xnRow += 1
                If CellData(xnRow, 1) <> "SSL" Then
                    Exit Do
                End If
            Loop

            xoExWB.Close()
            xoExcel.Quit()
            xoExWS = Nothing
            xoExWB = Nothing
            xoExcel = Nothing

            txtOutput.Text += "Done." & vbCrLf
            Cursor = Cursors.Default


            'MessageBox.Show("Done")
            If xsRtnMsg.Length > 0 Then
                MessageBox.Show(xsRtnMsg)
            End If

        Catch ex As Exception
            Cursor = Cursors.Default
            MessageBox.Show("Error: " & ex.Message & ": " & xsRtnMsg)
        Finally
            If Not IsNothing(DBConn) AndAlso DBConn.State <> ConnectionState.Closed Then
                DBConn.Close()
            End If

        End Try

    End Sub

   
    Private Sub btnClothingImportShrink_Click(sender As System.Object, e As System.EventArgs) Handles btnClothingImportShrink.Click


        '   Import data into Sansbury's Clothing Staging table

        Dim xnWSCount As Integer = 0
        Dim xsRtnMsg As String = ""
        Dim xnIPRowCount As Integer = 0
        Dim xnAddCount As Integer = 0

        Dim xadDates() As Date = Nothing
        Dim xanDateCol() As Integer = Nothing
        Dim xanDateValSales() As Decimal = Nothing
        Dim xasDateCodeCol() As String = Nothing
        'Dim xanDateValExposure() As Decimal = Nothing
        Dim xanDateValClExpAdj() As Decimal = Nothing
        Dim xanDateValRGISExpAdj() As Decimal = Nothing
        Dim xasCountYear() As String = Nothing
        Dim xasPeriodNo() As String = Nothing

        Try

            txtOutput.Text = "Output" & vbCrLf

            '   Identify the file to import
            dlgFileOpen.FileName = txtFileNameAndPathIPClothing.Text
            dlgFileOpen.ShowDialog()
            txtFileNameAndPathIPClothing.Text = dlgFileOpen.FileName

            If txtFileNameAndPathIPClothing.Text.Length = 0 Then
                MessageBox.Show("Missing file name. Job cancelled.")
                Return
            End If

            If Not File.Exists(txtFileNameAndPathIPClothing.Text) Then
                MessageBox.Show("File name not found. Job cancelled.")
                Return
            End If

            '   Get the DB Connection string
            '   Currently this is via the Radio buttons to vary Dev/Live. The client may only be Sainsburys
            'Dim xsClientName As String
            'If radDev.Checked Then
            '    xsClientName = "SSDev"
            'Else
            '    xsClientName = "SS"
            'End If

            Dim xbValid As Boolean = False
            Dim xoLic = New RGISLic.LicVal

            'If xoLic.GetDBForClient(xsClientName, "Data Source=smartspacedatacenter.com\BOE140;Initial Catalog=RGIS_Dashboard; Integrated Security=False; User ID=sa; Password=CjC7vHYB2yESaBgq") Then
            '    xbValid = True
            'Else
            '    '   If bad, then exit
            '    MessageBox.Show("Error getting the DB details: " & xoLic.RtnMsg)
            '    Exit Sub
            'End If

            ''   Build the connection string
            'Dim xsConnStr As String = "Data Source=" & xoLic.DBLocn & ";Initial Catalog=" & xoLic.DBName & "; Integrated Security=False; User ID=sa; Password=CjC7vHYB2yESaBgq"
            'Dim xsConnStr As String = "Server=CND42412H2\STPL_08;Initial Catalog=Sainsburys;Integrated Security=SSPI;"
            xbValid = True
            '   Clear the work tables
            DBConn = New SqlConnection(mConn)
            Dim xoCmd As New SqlCommand

            DBConn.Open()


            '   Open the workbook
            Dim xoExcel As Excel.Application
            Dim xoExWS As New Excel.Worksheet
            Dim xoExWB As Excel.Workbook

            StatusBarText = "Opening Excel file. "
            Cursor = Cursors.WaitCursor

            xoExcel = New Excel.Application

            xoExWB = xoExcel.Workbooks.Open(txtFileNameAndPathIPClothing.Text)
            xoExcel.Visible = True
            xoExcel.Application.DisplayAlerts = False

            StatusBarText = "Processing the worksheets."

            ' Jeremy Rogers - 15/08/2017
            ' Changed code so that it only reads data from last sheet in the Workbook
            xoExWS = xoExWB.Worksheets(xoExWB.Worksheets.Count)


            Dim xsWSName As String = xoExWS.Name
            Dim xbDatesFound As Boolean = False
            Dim xnDateCount As Integer = 0

            ReDim xadDates(xnDateCount)
            ReDim xanDateCol(xnDateCount)
            ReDim xasDateCodeCol(xnDateCount)
            ReDim xanDateValSales(xnDateCount)
            'ReDim xanDateValExposure(xnDateCount)
            ReDim xanDateValClExpAdj(xnDateCount)
            ReDim xanDateValRGISExpAdj(xnDateCount)
            ReDim xasCountYear(xnDateCount)
            ReDim xasPeriodNo(xnDateCount)

            '   Determine the sheet type
            Dim xnWrk As Integer = 0
            txtOutput.Text += xoExWS.Name & " " & vbCrLf
            StatusBarText = "Processing worksheet " & xoExWS.Name & "."

            '   Get the date range
            Dim xsDate As String = ""
            Dim xnDateStartCol As Integer = 6
            Dim xnDateStartRow As Integer = 1
            Dim xnDateEndCol As Integer = 0
            Dim xnRow As Integer = 1
            Dim xnCol As Integer = 1
            Dim xnColForThisDate As Integer = 0

            Dim CellData As Object(,) = xoExWS.UsedRange.Value

            '   Find the "Day" row
            '   The word "Day" appears in the cell preceding the row of dates. This can be in any row from 5-10 and any column from E(5) to K(11)
            xnRow = 5
            Dim xbFound As Boolean = False
            For xnRow = 5 To 10
                For xnCol = 5 To 11
                    If Not IsNothing(CellData(xnRow, xnCol)) AndAlso CellData(xnRow, xnCol).ToString.ToUpper = "DAY" Then
                        xbFound = True
                        Exit For
                    End If
                Next
                If xbFound Then Exit For
            Next

            '   Confirm that the word "Reason" is two cells below this. We use this to deduce what type of shrink transaction is being given
            If IsNothing(CellData(xnRow + 2, xnCol)) OrElse CellData(xnRow + 2, xnCol).ToString.ToUpper <> "REASON" Then
                Cursor = Cursors.Default
                MessageBox.Show("The 'Reason' row wasn't found two rows below 'Day'. The worksheet " & xsWSName & " is not in the correct format")
                Exit Sub
            End If


            '   Now extract the dates from the cells. There can be 1-3 columns per date with no rhyme or reason as to why (!)
            '   We need to record what date is relevant to each column so that the figures on each row can be totalled for the table update.
            '   The assumption is that all columns for a given date are to be added together and that they appear together (i.e. are not jumbled up in the column order)
            xnDateStartCol = xnCol + 1
            xnDateStartRow = xnRow
            Dim xdsvDate As Date = Nothing
            Dim xdWrk As Date = Nothing
            xnDateCount = -1

            For xnCol = xnDateStartCol To 30

                If IsNothing(CellData(xnRow, xnCol)) Then
                    Exit For
                End If

                xsDate = CellData(xnRow, xnCol).ToString()
                If xsDate.Length = 0 Or Not IsDate(xsDate) Then
                    Exit For
                End If

                xdWrk = CDate(xsDate)
                If IsNothing(xdsvDate) OrElse xdsvDate <> xdWrk Then

                    xnDateCount += 1
                    ReDim Preserve xadDates(xnDateCount)
                    Dim xdDate As Date = CDate(xsDate)
                    xadDates(xnDateCount) = xdDate
                    xbDatesFound = True
                    xdsvDate = xdWrk

                    '   Get CurrentYear/Period from date
                    ReDim Preserve xasCountYear(xnDateCount)
                    ReDim Preserve xasPeriodNo(xnDateCount)

                    Try
                        xoCmd = New SqlCommand
                        xoCmd.Connection = DBConn
                        xoCmd.CommandType = CommandType.StoredProcedure

                        xoCmd.CommandText = "p_GetPeriodAndCYForDate"
                        xoCmd.Parameters.Add("InputDate", SqlDbType.Date).Value = xadDates(xnDateCount)
                        xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
                        xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
                        xoCmd.ExecuteNonQuery()

                        Dim xsCurrentCountYear As String = ""
                        Dim xsWrk As String = xoCmd.Parameters("Count_Year").Value.ToString
                        If xsWrk.Length > 0 Then
                            xasCountYear(xnDateCount) = xsWrk
                        End If

                        Dim xsCurrentPeriod As String = ""
                        xsWrk = xoCmd.Parameters("Period_No").Value.ToString
                        If xsWrk.Length > 0 Then
                            xasPeriodNo(xnDateCount) = xsWrk
                        End If

                    Catch ex As Exception
                        xsRtnMsg += "Problem when processing header data. Error " & ex.Message & " "
                    End Try
                End If

                ReDim Preserve xanDateCol(xnCol)
                xanDateCol(xnCol) = xnDateCount

                ReDim Preserve xasDateCodeCol(xnCol)
                xasDateCodeCol(xnCol) = CellData(xnRow + 2, xnCol).ToString()

            Next

            xnDateEndCol = xnCol

            If Not xbDatesFound Then
                xsRtnMsg += "No dates found on worksheet " & xsWSName
                txtOutput.Text += "No dates found in " & xsWSName & vbCrLf
                ' Need to break out function ?
            End If

            '   Extract the data
            Dim xnRowCount As Integer = xoExWS.UsedRange.Rows.Count

            xnRow = 1
            Do While CellData(xnRow, 1) <> "SSL" And xnRow < xnRowCount
                xnRow += 1
            Loop

            If xnRow >= xnRowCount Then
                txtOutput.Text += "No data found in " & xsWSName & vbCrLf
            End If

            '   First data row found so process all the following non blank rows
            Do While xnRow < xnRowCount And CellData(xnRow, 1).Length > 0
                Dim xdDate As Date? = Nothing
                Dim xsStore As String = ""
                Dim xnStore As Integer = 0

                xnIPRowCount += 1

                'xnRow += 1
                xsRtnMsg = "a" & xnRow

                xsStore = CellData(xnRow, 4)
                If Not IsNothing(xsStore) And xsStore.Length > 0 Then
                    xnStore = CInt(xsStore)
                    If xnStore > 0 Then
                        '   Get the figure for each day
                        '   Note that (at this time) there will be an output row for each day, even for zero values
                        '   The s-p Inserts if there's no row there or updates if one does exist
                        '   We work through all columns to accumulate date and then output all dates at one time

                        ReDim xanDateValClExpAdj(xadDates.GetUpperBound(0))
                        ReDim xanDateValRGISExpAdj(xadDates.GetUpperBound(0))

                        For xnCol = xnDateStartCol To xnDateEndCol - 1

                            Dim xnVal As Decimal = 0
                            Dim xsWrk As String = CellData(xnRow, xnCol)
                            If Not IsNothing(xsWrk) AndAlso xsWrk.Length > 0 Then
                                xnVal = Val(xsWrk)
                            End If

                            'xanDateValExposure(xanDateCol(xnCol)) += xnVal
                            Select Case xasDateCodeCol(xnCol)
                                Case Is = "900" : xanDateValClExpAdj(xanDateCol(xnCol)) += xnVal
                                Case Is = "902" : xanDateValClExpAdj(xanDateCol(xnCol)) += xnVal
                                Case Is = "903" : xanDateValRGISExpAdj(xanDateCol(xnCol)) += xnVal
                            End Select

                        Next

                        For xnWrk = 0 To xnDateCount '- 1

                            If DBConn.State = ConnectionState.Closed Then
                                DBConn.Open()
                            End If

                            xoCmd = New SqlCommand
                            xoCmd.Connection = DBConn
                            xoCmd.CommandType = CommandType.StoredProcedure

                            Try
                                xoCmd.CommandText = "p_StageClothingInsertOrUpdate"
                                xoCmd.Parameters.Add("MetricDate", SqlDbType.DateTime).Value = xadDates(xnWrk)
                                xoCmd.Parameters.Add("StoreNumber", SqlDbType.Int).Value = xnStore
                                ' xoCmd.Parameters.Add("TotalSales", SqlDbType.Decimal).Value = Nothing
                                xoCmd.Parameters.Add("ClientExposureAdj", SqlDbType.Decimal).Value = xanDateValClExpAdj(xnWrk)
                                xoCmd.Parameters.Add("RGISExposureAdj", SqlDbType.Decimal).Value = xanDateValRGISExpAdj(xnWrk)
                                xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xasCountYear(xnWrk)
                                xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar).Value = xasPeriodNo(xnWrk)
                                xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output
                                'xoCmd.Parameters.Add("SqlRtn", SqlDbType.NVarChar).Direction = ParameterDirection.Output
                                xoCmd.ExecuteNonQuery()

                                Dim xsWrk As String = xoCmd.Parameters("RowsUpdated").Value.ToString
                                If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then
                                    Dim xnWrk2 As Integer = CInt(xsWrk)
                                    If xnWrk2 = 1 Then
                                        xnAddCount += 1
                                    Else
                                        xsRtnMsg += "Problem during p_StageClothingInsert/Update. "
                                        xbValid = False
                                    End If
                                End If
                            Catch ex As Exception
                                xsRtnMsg += "Problem when updating data for Row " & xnRow & " in sheet " & xsWSName & ". Error " & ex.Message & " "
                            End Try

                        Next


                        xsRtnMsg += "c"

                    End If
                End If
                xnRow += 1
                If CellData(xnRow, 1) <> "SSL" Then
                    Exit Do
                End If
            Loop

            txtOutput.Text += "Worksheet Done. IP Rows=" & xnIPRowCount & ", Added=" & xnAddCount & "." & vbCrLf
            xnIPRowCount = 0
            xnAddCount = 0

            xoExWB.Close()
            xoExcel.Quit()
            xoExWS = Nothing
            xoExWB = Nothing
            xoExcel = Nothing

            txtOutput.Text += "Done. " & vbCrLf
            Cursor = Cursors.Default
            'MessageBox.Show("Done")

        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message & ": " & xsRtnMsg)
        Finally
            If Not IsNothing(DBConn) AndAlso DBConn.State <> ConnectionState.Closed Then
                DBConn.Close()
            End If

        End Try

    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        StatusBarText = ""

        'P:\Client27_Sainsburys\Client27_Sainsburys\Dashboard\Dashboard_Phase2\ClientData

        Dim xsWrk As String = Application.ExecutablePath
        If xsWrk = "P:\Client27_Sainsburys\Client27_Sainsburys\Dashboard\Dashboard_Phase2\ClothingImporter\ClothingImporter\ClothingImporter\bin\Debug\ClothingImporter.EXE" Then
            txtFileNameAndPathIPClothing.Text = "P:\Client27_Sainsburys\Client27_Sainsburys\Dashboard\Dashboard_Phase2\ClientData\clothing 2012 sales3.xls"
        End If

        If Not LoadSavedConnectionDetailsFromRegistry() Then
            MsgBox("Please configure database from Dashboard load application.", MsgBoxStyle.Critical, "Clothing Importer")
            End
        End If

    End Sub

    Private Function LoadSavedConnectionDetailsFromRegistry() As Boolean
        Dim bOK As Boolean = False

        Try
            Dim sKeyName As String = "Dashboard_Config"
            Dim sSectionName As String = "Settings"

            mConn = DecryptString(GetSetting(sKeyName, sSectionName, "ConnString", ""))

            If mConn.Length > 0 AndAlso CreateConnection(mConn) Then
                bOK = True
            End If

        Catch ex As Exception
        Finally
            LoadSavedConnectionDetailsFromRegistry = bOK
        End Try

    End Function

    Private Function CreateConnection(ByVal sConnString As String) As Boolean
        Dim bOk As Boolean = False
        Try

            Dim oConn As IDbConnection = Nothing

            oConn = New SqlConnection

            oConn.ConnectionString = sConnString
            oConn.Open()

            bOk = True

        Catch ex As Exception

        Finally
            CreateConnection = bOk
        End Try
    End Function

    Private Function DecryptString(ByVal encrString As String) As String
        Try
            Dim b As Byte() = Convert.FromBase64String(encrString)
            Dim decryptedConnectionString As String = System.Text.ASCIIEncoding.ASCII.GetString(b)
            Return decryptedConnectionString
        Catch
            Throw
        End Try
    End Function

    Private Sub btnSupercatVerifyAllFiles_Click(sender As System.Object, e As System.EventArgs) Handles btnSupercatVerifyAllFiles.Click

        Dim xsPath As String = ""

        '   Identify the folder that contains all the required files
        Try
            txtOutput.Text = "Import Supercat verifier:" & vbCrLf
            StatusBarText = "Identify the folder containing the Supercat verifier data files"

            '            txtFileNameAndPathSuperCat.Text = "C:\_WIP\MarchWeb\Clients\RGIS\SuperCatVerifier data\"
            ' txtFileNameAndPathSuperCat.Text = ""

            dlgFolder.Description = "Browse for SuperCatVerifier Source folder"

            dlgFolder.RootFolder = Environment.SpecialFolder.Desktop
            If sDefaultSuperCatFolder.Length Then
                dlgFolder.SelectedPath = txtFileNameAndPathSuperCat.Text
            End If


            ' dlgFolder.SelectedPath = "C:\_WIP\MarchWeb\Clients\RGIS\SuperCatVerifier data\"

            If Not dlgFolder.ShowDialog = Windows.Forms.DialogResult.OK Then
                Exit Sub
            Else
                txtFileNameAndPathSuperCat.Text = dlgFolder.SelectedPath


            End If

        Catch ex As Exception
            MessageBox.Show("Problem getting the input file1 . Job cancelled. Error: " & ex.Message)
            Return
        End Try

    End Sub

    Private Sub btnDashDataClothingRebuild_Click(sender As System.Object, e As System.EventArgs) Handles btnDashDataClothingRebuild.Click

        Dim xbValid As Boolean = False
        Dim xoLic = New RGISLic.LicVal
        Dim xsClientName As String
        Dim xsRtnMsg As String = ""

        StatusBarText = ""

        'If radDev.Checked Then
        '    xsClientName = "SSDev"
        'Else
        '    xsClientName = "SS"
        'End If

        'If xoLic.GetDBForClient(xsClientName, "Data Source=smartspacedatacenter.com\BOE140;Initial Catalog=RGIS_Dashboard; Integrated Security=False; User ID=sa; Password=CjC7vHYB2yESaBgq") Then
        '    xbValid = True
        'Else
        '    '   If bad, then exit
        '    MessageBox.Show("Error getting the DB details: " & xoLic.RtnMsg)
        '    Exit Sub
        'End If

        '   Build the connection string
        '        Dim xsConnStr As String = "Data Source=" & xoLic.DBLocn & ";Initial Catalog=" & xoLic.DBName & "; Integrated Security=False; User ID=sa; Password=CjC7vHYB2yESaBgq"

        'Dim xsConnStr As String = "Server=CND42412H2\STPL_08;Initial Catalog=Sainsburys;Integrated Security=SSPI;"
        StatusBarText = "Rebuilding the clothing data table."

        '   Clear the work tables
        DBConn = New SqlConnection(mConn)
        Dim xoCmd As New SqlCommand

        DBConn.Open()
        
        '   This routine runs the s-p that will recreate the DD_Clothing data
        xoCmd = New SqlCommand
        xoCmd.Connection = DBConn
        xoCmd.CommandTimeout = 0
        xoCmd.CommandType = CommandType.StoredProcedure

        Try
            xoCmd.CommandText = "p_Dashboard_Data_Clothing_Update"
            xoCmd.ExecuteNonQuery()

            MessageBox.Show("Done")

        Catch ex As Exception
            xsRtnMsg += "Problem when updating data. Error " & ex.Message & " "
            MessageBox.Show(xsRtnMsg)
        End Try

    End Sub



    Private Sub btnImportSuperCat_Click(sender As Object, e As EventArgs) Handles btnImportSuperCat.Click
        Dim xsPath As String
        Dim sOrridgePath As String
        Dim nFilesProcessed As Integer
        Dim nFilesToProcess As Integer
        Dim headerDate As Date
        Dim SundayDate, SaturdayDate As Date
        Dim nDaysToSubtract As Integer
        Dim bDateObtained As Boolean


        SaturdayDate = Now
        nDaysToSubtract = SaturdayDate.DayOfWeek + 1
        SaturdayDate = SaturdayDate.AddDays(-nDaysToSubtract)
        SundayDate = SaturdayDate.AddDays(-6)

        ' Copy Orridge files where the date in the header row falls within the date range
        sOrridgePath = "\\EU-FPS\Client_Data\Sainsburys\Dashboard\Orridge Supercat Verifier files"
        If Directory.Exists(sOrridgePath) Then
            Dim xasOrridgeFileList As String() = Directory.GetFiles(sOrridgePath, "*.out36")
            For Each sOrridgeFile As String In xasOrridgeFileList
                ' Load file
                Dim line As String
                Using reader As New StreamReader(sOrridgeFile)
                    line = reader.ReadLine()
                End Using
                bDateObtained = True
                Try
                    headerDate = Date.Parse(line.Substring(108, 9))
                Catch
                    Dim strFile = Microsoft.VisualBasic.Right(sOrridgeFile, Len(sOrridgeFile) - InStrRev(sOrridgeFile, "\"))
                    MessageBox.Show("The date in file " + strFile + " in " + sOrridgePath + " cannot be extracted")
                    bDateObtained = False
                End Try


                If bDateObtained And (headerDate >= SundayDate And headerDate <= SaturdayDate) Then
                    ' If date within file falls within range then copy it to the main Supercat Verifier folder
                    Dim strFile = Microsoft.VisualBasic.Right(sOrridgeFile, Len(sOrridgeFile) - InStrRev(sOrridgeFile, "\"))
                    FileSystem.FileCopy(sOrridgeFile, sDefaultSuperCatFolder + "\" + strFile)

                End If
            Next
        End If


        If txtFileNameAndPathSuperCat.Text.Length = 0 Then
            MessageBox.Show("Missing folder name. Job cancelled.")
            Return
        End If
        dlgFolder.SelectedPath = txtFileNameAndPathSuperCat.Text
        xsPath = dlgFolder.SelectedPath
        If Directory.Exists(xsPath) Then
            ' JR - 25/04/2017 - Ensure only .out36 files are selected for processing
            Dim xasFileList As String() = Directory.GetFiles(xsPath, "*.out36")

            '   Count the files in the folder and confirm this is correct
            If xasFileList.GetUpperBound(0) = 0 Then
                MessageBox.Show("No files found")
                Exit Sub
            Else
                '   Process all the files - one by one
                ' Turn on wait cursor to inform user that application is busy
                tspb.Visible = True
                Me.Cursor = Cursors.WaitCursor

                nFilesToProcess = xasFileList.Count

                For Each xsFile As String In xasFileList
                    txtFileNameAndPathSuperCat.Text = xsFile
                    txtOutput.Text += vbCrLf & "Processing " & xsFile
                    StatusBarText = "Processing " & xsFile
                    ProcessSuperCatVarifierData()
                    nFilesProcessed += 1
                    tspb.Value = (nFilesProcessed / nFilesToProcess) * 100

                Next
                Me.Cursor = Cursors.Default
                MessageBox.Show("Done")
                tspb.Visible = False

            End If
        Else
            MessageBox.Show("Entered folder does not exist, please re-select.", "Clothing Importer", MessageBoxButtons.OK, MessageBoxIcon.Warning)


        End If


    End Sub

    Private Sub txtFileNameAndPathSuperCat_TextChanged(sender As Object, e As EventArgs) Handles txtFileNameAndPathSuperCat.TextChanged
        If txtFileNameAndPathSuperCat.TextLength > 0 Then
            btnImportSuperCat.Enabled = True
        Else
            btnImportSuperCat.Enabled = False
        End If
    End Sub

    Private Sub frmMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        ' Save selected DefaultSuperCatFolder value back to App.config

        If sDefaultSuperCatFolder <> txtFileNameAndPathSuperCat.Text Then
            ConfigurationManager.AppSettings.Set("DefaultSuperCatFolder", txtFileNameAndPathSuperCat.Text)

            Dim config As System.Configuration.Configuration
            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal)
            ' Add an Application Setting.
            config.AppSettings.Settings.Remove("DefaultSuperCatFolder")
            config.AppSettings.Settings.Add("DefaultSuperCatFolder", txtFileNameAndPathSuperCat.Text)
            'Save the configuration file.
            'config.Save(ConfigurationSaveMode.Modified)
            ' Force a reload of a changed section.
            'ConfigurationManager.RefreshSection("appSettings")
            e.Cancel = False

        End If
    End Sub
End Class