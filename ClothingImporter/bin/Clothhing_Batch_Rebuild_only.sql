declare	@start_date datetime
declare	@end_date datetime
declare	@batch_Size int
Set @Start_date='2013-03-17 00:00:00.000'
Set @End_date='2014-10-05 00:00:00.000'
set @Batch_Size=20

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Declare @proc_Start datetime
Declare @proc_end datetime
Declare @work_start datetime
Declare @work_end datetime
Declare @Batch_Ct int
Declare @Curr_RowCt int
Declare @Curr_RowCt_TA int
Declare @Prev_RowCt int
Select @Curr_RowCt= isnull(Count(id),0) from TB_Dashboard_Data_Clothing
Select @Curr_RowCt_TA= isnull(Count(id),0)from TB_Dashboard_Data_Clothing_TA
Set @Prev_RowCt=@Curr_RowCt+@Prev_RowCt

Set @Batch_Ct=0

--Store content of process_dates table so we can reset at the end.
select @proc_Start=Process_Start_Date,@proc_End=Process_End_Date from Process_dates
update Process_Dates set Batch_Rebuild=1


Set @work_start=@start_date
Set @work_end=(DATEADD(dd,@batch_Size,@work_start))

While @work_start< @end_date 
Begin
	Set @Batch_Ct=@Batch_Ct+1
	Set @work_end=(DATEADD(dd,@batch_Size,@work_start))
	if @work_end>@end_date set @work_end=@end_date
	update Process_Dates set Process_Start_Date=@work_start, Process_End_Date=@work_end
--	Select * from Process_Dates
-- call the main SP here
	exec SP_Dashboard_Clothing_Data_update
	Set @work_start=(DATEADD(dd,1,@work_end))
	Select @Curr_RowCt= isnull(Count(id),0) from TB_Dashboard_Data_Clothing
	Select @Curr_RowCt_TA= isnull(Count(id),0) from TB_Dashboard_Data_Clothing_TA
	
	insert into TB_Rebuild_Log (Batch_Number,EndTime,RowCt) values(@Batch_Ct,GETDATE(),@Curr_RowCt+@Curr_RowCt_TA-@Prev_RowCt)
	Set @Prev_RowCt=@Curr_RowCt+@Curr_RowCt_TA
end

--having completed our run then reset the process_dates table
update Process_Dates set Process_Start_Date=@proc_start, Process_End_Date=@Proc_end,Batch_Rebuild=0
--Select * from Process_Dates
--Recreate the reports etc based upon rebuilt data